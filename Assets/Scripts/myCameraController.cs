﻿using UnityEngine;

public class myCameraController : MonoBehaviour {
    public Transform tTarget;

    public float fSmoothSpeed = 0.5f;
    public Vector3 vOffset;

    private Vector3 vDesiredPosition;
    private Vector3 vSmoothedPosition;

    void FixedUpdate()
    {
//        transform.position = tTarget.position + vOffset;

        vDesiredPosition = tTarget.position + vOffset;
        vSmoothedPosition = Vector3.Lerp(transform.position, vDesiredPosition, fSmoothSpeed * Time.deltaTime);
        transform.position = vSmoothedPosition;

        transform.LookAt(tTarget);
    }

}
