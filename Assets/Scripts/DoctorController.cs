﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoctorController : MonoBehaviour {

    public Transform[] aWayPoints;
    public float fSpeed;
    public float fTurnSpeed;

    public Animator MyAnimator;

    private Vector3 vDestination;
    private int iWPIndex = 0;
    private MoveToDestination moveDestination;
//    private bool bFirstTime = true;

    //Referencias a las variables del Animator
    private int walkingHash;
    private int wavingHash;
    private int scaringHash;
    private int talkingHash00;

    private Vector3 vTargetDirection;

    public enum CharacterStatus
    {
        IDLE,
        IDLE2,
        WALKING,
        TALKING00,
        TALKING01,
        TALKING02,
        HELLO,
        SCARED  
    }

    CharacterStatus myStatus;
    // Use this for initialization

    void OnColliderEnter(BoxCollider other)
    {
        if( other.tag == "Finish")
        {
            moveDestination.setArrived(true);
            moveDestination.setMove(false);
            Walk();
        }
    }

    void Wave()
    {
        //        MyAnimator.SetBool("Waving", true);
//        Debug.Log("Estoy saludando");
       StartCoroutine(waitAndChangeState(CharacterStatus.IDLE2, 3));
  //      Invoke("Wave", 5f);
    }

    void Walk()
    {
        //        if (moveDestination.getArrived() == true && bFirstTime == true)
        if (moveDestination.getArrived() == true)
        {
//            Debug.Log("He llegado al destino");
//            bFirstTime = false;
            moveDestination.setArrived(false);
            moveDestination.setMove(false);
            newDestination();
            //            StartCoroutine(waitAndChangeDestination());
        }

        vTargetDirection = aWayPoints[iWPIndex].transform.position - this.transform.position;
        this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.LookRotation(vTargetDirection), Time.time * fTurnSpeed);
    }

    void Talk00()
    {
//        Debug.Log("Estoy hablando 00");
        StartCoroutine(waitAndChangeState(CharacterStatus.TALKING01, 2));
    }

    void Talk01()
    {
//        Debug.Log("Estoy hablando 00");
        StartCoroutine(waitAndChangeState(CharacterStatus.IDLE2, 2));
    }


    public void newDestination()
    {
        iWPIndex++;
        if (iWPIndex >= aWayPoints.Length)
        {
            iWPIndex = 0;
//            stateController(CharacterStatus.HELLO);
            stateController(CharacterStatus.IDLE2);
        }
        else
        {
            //        bFirstTime = true;
            vDestination = aWayPoints[iWPIndex].position;
            moveDestination.setDestino(vDestination);
            stateController(CharacterStatus.IDLE);
        }
    }

    public void IdleState(CharacterStatus newState, int iSeconds)
    {
        StartCoroutine(waitAndChangeState(newState, iSeconds));
    }

    public void stateController(CharacterStatus newState)
    {
        myStatus = newState;

//        Debug.Log("Estoy en el estado: " + myStatus);
        switch (myStatus)
        {
            case (CharacterStatus.IDLE):
                MyAnimator.SetBool(wavingHash, false);
                MyAnimator.SetBool(walkingHash, false);
                MyAnimator.SetBool(scaringHash, false);
                MyAnimator.SetInteger(talkingHash00, 0);

                moveDestination.setMove(false);

                IdleState(CharacterStatus.WALKING, Random.Range(3, 5));
                break;
            case (CharacterStatus.IDLE2):
                MyAnimator.SetBool(wavingHash, false);
                MyAnimator.SetBool(walkingHash, false);
                MyAnimator.SetBool(scaringHash, false);
                MyAnimator.SetInteger(talkingHash00, 0);

                StartCoroutine(waitAndChangeState(CharacterStatus.TALKING00, Random.Range(2, 5)));
                break;

            case (CharacterStatus.WALKING):
                MyAnimator.SetBool(wavingHash, false);
                MyAnimator.SetBool(scaringHash, false);
                MyAnimator.SetBool(walkingHash, true);
                MyAnimator.SetInteger(talkingHash00, 0);

                moveDestination.setMove(true);
//                this.transform.LookAt(aWayPoints[iWPIndex]);
                Walk();
                break;

            case (CharacterStatus.TALKING00):
                MyAnimator.SetBool(wavingHash, false);
                MyAnimator.SetBool(walkingHash, false);
                MyAnimator.SetBool(scaringHash, false);
                MyAnimator.SetInteger(talkingHash00, 1);
                Talk00();
                break;

            case (CharacterStatus.TALKING01):
                MyAnimator.SetBool(wavingHash, false);
                MyAnimator.SetBool(walkingHash, false);
                MyAnimator.SetBool(scaringHash, false);
                MyAnimator.SetInteger(talkingHash00, 2);
                Talk01();
                break;

            case (CharacterStatus.TALKING02):
                break;

            case (CharacterStatus.HELLO):
                MyAnimator.SetBool(walkingHash, false);
                MyAnimator.SetBool(scaringHash, false);
                MyAnimator.SetBool(wavingHash, true);
                Wave();
                break;

            case (CharacterStatus.SCARED):
                break;
        }
    }


    #region Corrutinas
    IEnumerator waitAndChangeState(CharacterStatus newState, int iSeconds)
    {
//        Debug.Log("Voy a esperar estos segundos: " + iSeconds);
        yield return new WaitForSeconds(iSeconds);
        stateController(newState);
    }

    IEnumerator waitAndChangeDestination()
    {
//        Debug.Log("He entrado en el cambio de destino");
        yield return new WaitForSeconds(3);
//        bFirstTime = true;
        newDestination();
    }
    #endregion


    void Awake()
    {
//        MyAnimator = this.GetComponent<Animator>();
        walkingHash = Animator.StringToHash("Walking");
        wavingHash = Animator.StringToHash("Waving");
        scaringHash = Animator.StringToHash("Scared");
        talkingHash00 = Animator.StringToHash("Talking");

        vDestination = aWayPoints[iWPIndex].position;

        moveDestination = this.GetComponent<MoveToDestination>();

        //Primer destino para el walking. El personaje está parado hasta que setMove sea true.
        moveDestination.setMove(false);
        moveDestination.setArrived(true);
        moveDestination.setSpeed(fSpeed);
        moveDestination.setDestino(vDestination);
    }

    void Start () {
        stateController(CharacterStatus.IDLE);
    }

    void Update()
    {
        if(myStatus == CharacterStatus.WALKING)
        {
            Walk();
        }
    }
}
