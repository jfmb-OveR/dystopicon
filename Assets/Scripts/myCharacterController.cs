﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myCharacterController : MonoBehaviour {

    public float fInputDelay = 0.1f;
    public float fRunningThreshold = 0.5f;
    public float fWalkSpeed = 3f;
    public float fRunSpeed = 5f;
    public float fTurnSpeed = 3f;


    [HideInInspector]
    public enum characterState
    {
        IDLE,
        WALKING,
        RUNNING
    }

    private characterState state;

    private Animator characterAnimator;
    private int walkingHash;

    private Rigidbody rbCharacter;
    private Transform tCharacter;
    private Vector3 vDirection = new Vector3();
//    private Vector3 vDirectionY = new Vector3();
    private Quaternion qCharacterRotation;

    private float fHorinzotalInput;
    private float fVerticalInput;

    private bool bIsWalking = false;
    private bool bStop = false;

    private float fTemp = 0f;

    void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "obstacle")
        {
            setNewState(characterState.IDLE, 0f);
            bStop = true;
            rbCharacter.MovePosition(tCharacter.position + Vector3.zero * fRunSpeed * Time.deltaTime);

            Debug.Log("Colisión!!!");
            StartCoroutine(corrutineStop());
        }
    }

    public Quaternion getRotation()
    {
        return tCharacter.rotation;
    }

    public void setNewState(characterState newState, float fNewValue)
    {
//        float fThreshold = 0f;
        if(state != newState)
        {
            state = newState;
            switch (state)
            {
                case characterState.IDLE:
                    bIsWalking = false;
                    fTemp = 0f;
                    break;
                case characterState.WALKING:
                    isWalking();
                    break;
                case characterState.RUNNING:
                    isRunning();
                    break;
            }
        }
        characterAnimator.SetFloat(walkingHash, fNewValue);
    }

    public void isIdle()
    {
    }

    public void isWalking()
    {
    }

    public void isRunning()
    {
    }

    public void getInput()
    {
        fHorinzotalInput = Input.GetAxis("Horizontal");
        fVerticalInput = Input.GetAxis("Vertical");

        //        if (Input.GetKey(KeyCode.LeftShift) || ((Mathf.Abs(fHorinzotalInput) + Mathf.Abs(fVerticalInput))*0.25f < fRunningThreshold))
        if (Input.GetKey(KeyCode.LeftShift))
            bIsWalking = true;
        else
            bIsWalking = false;

    }

    public void turnCharacter()
    {
//        Vector3 newDirection = new Vector3(fHorinzotalInput, 0,fVerticalInput);
        if (vDirection != Vector3.zero)
            tCharacter.rotation = Quaternion.RotateTowards(tCharacter.rotation, Quaternion.LookRotation(vDirection), Time.time * fTurnSpeed);

//        qCharacterRotation = Quaternion.FromToRotation(tCharacter.forward, vDirectionX + vDirectionY);
//        tCharacter.rotation = Quaternion.Lerp(tCharacter.rotation, qCharacterRotation, fTurnSpeed * Time.deltaTime);
    }

    public void moveCharacter()
    {
        vDirection = new Vector3(fHorinzotalInput, 0.0f, fVerticalInput);
//        Debug.Log("Vector de dirección: " + vDirection);

        fTemp = Mathf.Abs(fHorinzotalInput) + Mathf.Abs(fVerticalInput);

        if (fTemp > fInputDelay)
        {
            //            if ((Mathf.Abs(fTemp) < fRunningThreshold) || (Input.GetKey(KeyCode.LeftShift)))
            if (bIsWalking == true)
            {
//                Debug.Log("Estoy andando!");
                //                rbCharacter.AddForce(vDirection * fWalkSpeed);
                //                rbCharacter.velocity = tCharacter.forward * fTemp * fWalkSpeed;
                rbCharacter.MovePosition(tCharacter.position + vDirection * fWalkSpeed * Time.deltaTime);
                setNewState(characterState.WALKING, fTemp*0.25f);
            }
            else
            {
                //                rbCharacter.AddForce(vDirection * fRunSpeed);
                //                rbCharacter.velocity = tCharacter.forward * fTemp *  fRunSpeed;
                rbCharacter.MovePosition(tCharacter.position + vDirection * fRunSpeed * Time.deltaTime);
                //Debug.Log("Estoy corriendo!");
                setNewState(characterState.RUNNING, fTemp);
            }
        }
        else
        {
            setNewState(characterState.IDLE, 0f);
//            rbCharacter.velocity = Vector3.zero;
        }
    }


    IEnumerator corrutineStop()
    {
        yield return new WaitForSeconds(3);
        bStop = false;
    }

    void Awake()
    {
        characterAnimator = this.GetComponent<Animator>();
        walkingHash = Animator.StringToHash("walking");

        rbCharacter = this.GetComponent<Rigidbody>();
        tCharacter = this.GetComponent<Transform>();
    }

    // Use this for initialization
    void Start () {
        state = characterState.IDLE;

        fHorinzotalInput = fVerticalInput = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (bStop == false)
        {
            getInput();
            turnCharacter();
        }
        else
            Debug.Log("eSTOY PARADO!!!");
    }

    void FixedUpdate()
    {
        moveCharacter();
    }
}
