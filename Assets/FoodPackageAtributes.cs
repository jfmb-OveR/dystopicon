﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodPackageAtributes : MonoBehaviour {
    public dystopicSceneManager mySceneManager;
    public characterManager myCharacterManager;
    public GameObject goParent;
    public GameObject goSoundOpen;
    public GameObject goLabelPackage;

    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;

    [Header("0:health, 1: hunger, 2:comfort, 3: fun")]
    public float[] aValueModifier; //0: Health, 1: Hunger, 2: Comfort, 3: fun
    [Header("Añade dinero a la cuenta, valor negativo")]
    public float fMoneyToAdd; //Dinero que se va a añadir a la cuenta. Valor en negativo.

    private void OnMouseEnter()
    {
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);

        goLabelPackage.SetActive(true);
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);
        goLabelPackage.SetActive(false);
    }
    void OnMouseDown()
    {
        myCharacterManager.receiveModifiers(aValueModifier, 1);
        myCharacterManager.restaMoney(fMoneyToAdd);
        goSoundOpen.SetActive(true);
        goLabelPackage.SetActive(false);
        StartCoroutine(CorDestroyPackage());
    }

    IEnumerator CorDestroyPackage()
    {
        yield return new WaitForSeconds(1);
        Destroy(goParent);
    }

    IEnumerator CorDestroyPackageIfNotUsed()
    {
        yield return new WaitForSeconds(20);
        if (this.gameObject != null)
        {
            Destroy(goParent);
        }
    }

    private void Start()
    {
        goLabelPackage.SetActive(false);
        StartCoroutine(CorDestroyPackageIfNotUsed());
    }
}
