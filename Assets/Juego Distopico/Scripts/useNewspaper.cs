﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class useNewspaper : MonoBehaviour {
    public myAnalyticsScriptForDevices myAnalytics;

    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;

    public dystopicSceneManager mySceneManager;
    public characterManager myCharacterManager;
    public AudioSource audioSoundOnMouseOver;
    public useTV_v2 myTV;

    public UILabel labelDevice;
    public UILabel labelBreakingNews;

    private Material matObject;
    private bool bOnMouseOverFirstTime = true;

    public void ResetMaterial()
    {
        matObject.EnableKeyword("_EMISSION");
        labelBreakingNews.enabled = true;
    }

    private void OnMouseOver()
    {
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);

        //        textDevice.enabled = true;
        if (bOnMouseOverFirstTime == true)
        {
            audioSoundOnMouseOver.Play();
            bOnMouseOverFirstTime = false;
        }

        labelDevice.enabled = true;
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        //        textDevice.enabled = false;
        bOnMouseOverFirstTime = true;
        labelDevice.enabled = false;
    }

    void OnMouseDown()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        myAnalytics.increaseTimeUsed();

        myTV.setTVOff();
        myTV.hideChannelsImage();

        matObject.DisableKeyword("_EMISSION");
        labelBreakingNews.enabled = false;

        mySceneManager.showNewsFromToday();
//        Debug.Log("He hecho click!!!!");
    }

    private void Start()
    {
        labelDevice.enabled = false;

        matObject = this.GetComponent<Renderer>().material;
        ResetMaterial();
    }
}
