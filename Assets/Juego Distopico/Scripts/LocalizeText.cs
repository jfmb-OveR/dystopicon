﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizeText : MonoBehaviour {
    public Text textToLocalize;
    public string sKey;

	// Use this for initialization
	void Start () {
        textToLocalize.text = Localization.Get(sKey);
	}
}
