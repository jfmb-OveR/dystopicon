﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hideComics : MonoBehaviour {

    public GameObject goThis;
    public newsController myNewsController; //Sólo se usa en el cómic del atentado.
    public Blinds_Behaviour myBlinds;

    void OnMouseDown()
    {
//        dystopicGameManager.myGameManagerInstance.ResumeGame();
        Debug.Log("He apretado en el comic!!!!!!!");
    }

    private void ResumeGame()
    {
        dystopicGameManager.myGameManagerInstance.ResumeGame();
    }

    public void Hide()
    {
        int iChildCount = goThis.transform.childCount;
        for (int i = 0; i < iChildCount; i++)
        {
            goThis.transform.GetChild(i).transform.gameObject.SetActive(false);
        }

        goThis.SetActive(false);

        ResumeGame();
    }

    public void HideAtentado()
    {
        ResumeGame();
        StartCoroutine(CorSetEndGame());
    }

    IEnumerator CorSetEndGame()
    {

        yield return new WaitForSeconds(1);

        myNewsController.setEndGameTerroristStrike();
        goThis.SetActive(false);
    }
}
