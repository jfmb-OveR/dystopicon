﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weirdPackageScript : MonoBehaviour {
    public characterManager myCharacterManager;
    public dystopicSceneManager myDystopicSceneManager;
    //public newsController myNewsController;
    public GameObject goWeirdMessage;
    public GameObject goThis;
//    public GameObject goLabel;
    public GameObject goComicAtentado;

    public GameObject[] aGoPackages;
    public GameObject[] aPackagesMessages;

    public void FirstPackageKeepIt()
    {
        PackageKeepIt(0);
    }

    public void FirstPackageReportThem()
    {
        PackageReportThem(0);
    }

    public void SecondPackageKeepIt()
    {
        PackageKeepIt(1);
    }

    public void SecondPackageReportThem()
    {
        PackageReportThem(1);
    }

    public void ThirdPackageKeepIt()
    {
        PackageKeepIt(2);
    }

    public void ThirdPackageReportThem()
    {
        PackageReportThem(2);
    }

    public void FourthPackageKeepIt()
    {
        PackageKeepIt(3);
    }

    public void FourthPackageReportThem()
    {
        PackageReportThem(3);
    }

    public void ActivateMessage(int iIndex)
    {
//        goLabel.SetActive(true);
        aPackagesMessages[iIndex].SetActive(true);
        aGoPackages[iIndex].SetActive(false);

        StartCoroutine(showWeirdMessage(iIndex));
    }

    public void DesctivatePackage(int iIndex)
    {
        aGoPackages[iIndex].SetActive(false);
    }

    public void PackageKeepIt(int iIndex)
    {
        dystopicGameManager.myGameManagerInstance.ResumeGame();
        myCharacterManager.sumaPoliticalAffinity(-2f);
        //        goWeirdMessage.SetActive(false);

        myDystopicSceneManager.setSpecialEvent(iIndex, true);

        aPackagesMessages[iIndex].SetActive(false);
        aGoPackages[iIndex].SetActive(false);

        if (iIndex == 3)
        {
            StartCoroutine(CorShowTerroristStrikeComic());
        }
    }

    public void PackageReportThem(int iIndex)
    {
        dystopicGameManager.myGameManagerInstance.ResumeGame();
        myCharacterManager.sumaPoliticalAffinity(2f);

        aPackagesMessages[iIndex].SetActive(false);

        myDystopicSceneManager.setSpecialEvent(iIndex, false);

        aGoPackages[iIndex].SetActive(false);
    }

    IEnumerator showWeirdMessage(int iIndex)
    {
        yield return new WaitForSeconds(0.05f);
        dystopicGameManager.myGameManagerInstance.PauseGame();
        //        dystopicGameManager.myGameManagerInstance.PauseGame();
        //        goWeirdMessage.SetActive(false);

        //        goWeirdMessage.SetActive(true);
        aPackagesMessages[iIndex].SetActive(true);
    }

    IEnumerator CorShowTerroristStrikeComic()
    {
        goComicAtentado.SetActive(true);
        yield return new WaitForSeconds(0.05f);
        dystopicGameManager.myGameManagerInstance.PauseGame();
    }

    void Awake()
    {
//        goWeirdMessage.SetActive(false);

        for(int i = 0; i < aGoPackages.Length; i++)
        {
            aGoPackages[i].SetActive(false);
            aPackagesMessages[i].SetActive(false);
        }
//        goLabel.SetActive(false);
    }
}
