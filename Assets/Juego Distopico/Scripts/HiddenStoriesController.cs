﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HiddenStoriesController : MonoBehaviour {

    public string[] arrayStringReferences; //Referencias al archivo de Localización para leer la historia.
    public GameObject[] arrayObjects;
    public GameObject goStoriesBackground; //el objeto padre de las historias.
    public UILabel labelStory;
    public int iNumberOfObjects = 5; //Número de objetos que tienen historia oculta
    public GameObject goArmario; //Es el objeto final

    private int[] arrayOrderForObjects; //Guardaremos en cada elemento el índice del objeto que se va a activar 
    private bool[] arrayIndexUsed; //Cada elemento nos dice si ese índice ya ha salido.
    private int iStoriesCounter = 0;


    public void ActivateNewStory()
    {
        if(iStoriesCounter < iNumberOfObjects)
        {
//            Debug.Log("Activando el objeto: " + iStoriesCounter);
            arrayObjects[arrayOrderForObjects[iStoriesCounter]].GetComponent<HiddenStoryScript>().ActivateStar();
        }
        else
        {
            goArmario.GetComponent<ArmarioScript>().ActivateObject();
        }
    }

    public void ShowNewStory()
    {
        if(iStoriesCounter < arrayStringReferences.Length)
        {
            labelStory.text = Localization.Get(arrayStringReferences[iStoriesCounter]);
            iStoriesCounter++;
            StartCoroutine(showWeirdMessage());
        }
    }

    IEnumerator CorShowFirstStory()
    {
        yield return new WaitForSeconds(20);
        ActivateNewStory();
    }

    IEnumerator showWeirdMessage()
    {
        yield return new WaitForSeconds(0.05f);
        dystopicGameManager.myGameManagerInstance.PauseGame();
        goStoriesBackground.SetActive(true);
        StartCoroutine(CorShowFirstStory());
        //        dystopicGameManager.myGameManagerInstance.PauseGame();
        //        goWeirdMessage.SetActive(false);

        //        goWeirdMessage.SetActive(true);
    }

    private void Awake()
    {
        arrayOrderForObjects = new int[iNumberOfObjects];
        arrayIndexUsed = new bool[iNumberOfObjects];
    }

    private void Start()
    {
        int iNumberTemp;
        int iNumbersUsed = 0;

        for (int i = 0; i < iNumberOfObjects; i++)
        {
            arrayOrderForObjects[i] = -1;
            arrayIndexUsed[i] = false;
        }

        do
        {
            iNumberTemp = Random.Range(0, iNumberOfObjects);
            if(arrayIndexUsed[iNumberTemp] == false)
            {
                arrayIndexUsed[iNumberTemp] = true;
                arrayOrderForObjects[iNumbersUsed] = iNumberTemp;
                iNumbersUsed++;
            }
        } while (iNumbersUsed <= iNumberOfObjects-1);
        /*
                for (int i = 0; i < iNumberOfObjects; i++)
                {
                    Debug.Log("Objeto del índice " + i + " " + arrayOrderForObjects[i]);
                }
        */

        StartCoroutine(CorShowFirstStory());
    }
}
