﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class useRat : useDevice {

    public RatsController myRatController;

    private Animator animRat;
    private int iRatHash;

    public float getCost()
    {
        return fMoneyModifier;
    }

    private void OnMouseOver()
    {
        //        labelDevice.enabled = true;
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);

        labelDevice.SetActive(true);
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        //        labelDevice.enabled = false;
        labelDevice.SetActive(false);
    }

    void OnMouseDown()
    {
        myAnalytics.increaseTimeUsed();

        myTV.setTVOff();
        myTV.hideChannelsImage();

        myCharacterManager.restaMoney(fMoneyModifier); //El valor debe ser negativo para que sume
        myCharacterManager.receiveModifiers(aValueModifier, iSeconds);

        soundUse.Play();
        myRatController.DecreseRats();
        labelDevice.SetActive(false);
        StartCoroutine(closeDevice());
    }

    IEnumerator showHideText()
    {
        yield return new WaitForSeconds(1);        
    }

    IEnumerator closeDevice()
    {
        yield return new WaitForSeconds(0.3f);
        animRat.SetBool(iRatHash, true);

        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }

    private void Awake()
    {
        animRat = this.GetComponent<Animator>();
        iRatHash = Animator.StringToHash("openDevice");
        animRat.SetBool(iRatHash, false);
        //        labelDevice.enabled = false;
        labelDevice.SetActive(false);
    }
}
