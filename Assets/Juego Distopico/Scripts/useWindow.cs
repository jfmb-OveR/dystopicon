﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class useWindow : MonoBehaviour {

    public int iIndex;
    public myAnalyticsScriptForDevices myAnalytics;

    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;

    public float fPoliticalModifier; //+ suma afinidad, - resta afinidad
    //    public dystopicCharacterController myDystopicCharacterController;
    public dystopicSceneManager mySceneManager;
    public characterManager myCharacterManager;
    //    public Transform tPlayerDestination;
    public AudioSource audioSoundOnMouseOver;
    public useTV_v2 myTV;

    public UILabel labelDevices;

    public Blinds_Behaviour myBlinds;

    private bool bOnMouseOverFirstTime = true;

    private void OnMouseOver()
    {
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);

        if (bOnMouseOverFirstTime == true)
        {
            audioSoundOnMouseOver.Play();
            bOnMouseOverFirstTime = false;
        }

        labelDevices.enabled = true;
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        //        textDevice.enabled = false;
        bOnMouseOverFirstTime = true;
        labelDevices.enabled = false;
    }

    void OnMouseDown()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        myAnalytics.increaseTimeUsed();

        myTV.setTVOff();
        myTV.hideChannelsImage();

        //        myCharacterManager.setIdleState();
        //        myCharacterManager.setDeviceActive(iIndex, false);

        if (mySceneManager.GetWindowHasEventToday() == true)
        {
            myBlinds.OpenBlinds(true);
            StartCoroutine(CorShowComic());
        }
        else
            mySceneManager.showText(Localization.Get("WindowNothing"));
        //        myCharacterManager.setNewDestination(iIndex, tPlayerDestination.position);
    }

    IEnumerator CorShowComic()
    {
        yield return new WaitForSeconds(1);
        mySceneManager.showWindowEventsFromToday();
//        myCharacterManager.sumaPoliticalAffinity(fPoliticalModifier); //TO DO: no siempre debe bajar afinidad política!!!!!
        StartCoroutine(CorCloseBlinds());
    }

    IEnumerator CorCloseBlinds()
    {
        yield return new WaitForSeconds(5);
        myBlinds.OpenBlinds(false);
    }

    void Start()
    {
        labelDevices.enabled = false;
    }
}
