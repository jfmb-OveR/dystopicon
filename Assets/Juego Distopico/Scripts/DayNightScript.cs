﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightScript : MonoBehaviour {
    public dystopicSceneManager mySceneManager;
    private float fCycleSpeed;

    private Transform tLight;
    private Light lSunLight;

    private float fGradosGiro = 359f;

    public void SetCycleSpeed(float fNewValue)
    {
        fCycleSpeed = fNewValue;
    }

    public void ResetGradosGiro()
    {
        fGradosGiro = 359;
        Debug.Log("HE RESETEADO LOS GRADOS: " +fGradosGiro);
    }

	// Use this for initialization
	void Start () {
        fCycleSpeed = mySceneManager.GetDayVelocity();

        tLight = this.transform;
        lSunLight = this.GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        tLight.Rotate(Vector3.down * Time.deltaTime*fCycleSpeed);
//        Debug.Log("Valor de fGradosGiro: " + fGradosGiro + "\n Angulo euler" + tLight.rotation.eulerAngles.x);
        fGradosGiro -= Time.deltaTime * fCycleSpeed;
/*
        if (fGradosGiro < 1f)
        {
            fGradosGiro = 359;
            mySceneManager.StartNewDay();
        }
*/
        //        if (tLight.rotation.eulerAngles.x > 0f && tLight.rotation.eulerAngles.x <1f)
        //        {
        //            mySceneManager.StartNewDay();
        //        }
    }
}
