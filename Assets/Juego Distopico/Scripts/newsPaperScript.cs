﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newsPaperScript : MonoBehaviour {

    public GameObject prefabNewspaper;
    public Transform tStartPosition;
    public ParticleSystem particlesSteam;
    private AudioSource audioTubeSound;

//    private GameObject goNewspaper;
    private useNewspaper scriptNewsPaper;

    public void NewDayStarts()
    {
        scriptNewsPaper.ResetMaterial();

        //        matObject.EnableKeyword("_EMISSION");
        prefabNewspaper.transform.position = tStartPosition.position;
        audioTubeSound.Play();
        particlesSteam.Play();
    }

    private void Start()
    {
        audioTubeSound = this.GetComponent<AudioSource>();
        scriptNewsPaper = prefabNewspaper.GetComponent<useNewspaper>();
//        scriptNewsPaper.ResetMaterial();
//        matObject  = prefabNewspaper.GetComponent<Material>();
//        matObject.EnableKeyword("_EMISSION");
    }
}
