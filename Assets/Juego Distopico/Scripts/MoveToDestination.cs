﻿
//public class MoveToDestination : MonoBehaviour {
using UnityEngine;
using System.Collections;

public class MoveToDestination : MonoBehaviour
{

    private float fSpeed;
    private Vector3 vDestino;
    private bool bMove;
    private bool bArrived;

    public bool getArrived()
    {
        return bArrived;
    }

    public bool getIsMoving()
    {
        return bMove;
    }

    public void setMove(bool newValue)
    {
        bMove = newValue;
        bArrived = !bMove;
    }

    public void setArrived(bool newValue)
    {
        bArrived = newValue;
        bMove = !bArrived;
    }

    public void setDestino(Vector3 newDest)
    {
        vDestino = newDest;
        bArrived = false;
    }

    public void setSpeed(float newValue)
    {
        fSpeed = newValue;
    }

    // Use this for initialization
    void Awake()
    {
        bArrived = false;
        bMove = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (bMove == true)
        {
            if (vDestino != null)
            {
//                Debug.Log("Objeto en movimiento");
                this.transform.position = Vector3.MoveTowards(transform.position, vDestino, fSpeed * Time.deltaTime);
                if (transform.position == vDestino)
                {
                    bMove = false;
                    bArrived = true;
                }
            }
            else
            {
                Debug.Log("!!!!!Destino nulo!!!!");
            }
        }
        else
        {
//            bArrived = true;
//            Debug.Log("!!!!!El objeto está parado!!!!");
        }
    }
}
