﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class useDevice : MonoBehaviour {

    public int iIndex;
    public myAnalyticsScriptForDevices myAnalytics;

    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;

    //    public dystopicCharacterController myDystopicCharacterController;
    public dystopicSceneManager mySceneManager;
    public characterManager myCharacterManager;
    public float fMoneyModifier;
//    public float fValueModifier;
    [Header("0:health, 1: hunger, 2:comfort, 3: fun")]
    public float[] aValueModifier; //0: Health, 1: Hunger, 2: Comfort, 3: fun

    public float fPoliticalModifier; //+ añade afinidad, - quita afinidad. Se suma de golpe, no con el tiempo.
    public int iSeconds = 0;

    public AudioSource audioSoundMouseOver;

    public useTV_v2 myTV;

    //    public Text textDevice;
    //    public UILabel labelDevice;
    public GameObject labelDevice;

    public AudioSource soundUse;
    public AudioSource soundCoin;
    public Renderer materialLuzCandado;
    public Material materialVerdeCandado;
    public Material materialRojoCandado;
    public AudioSource soundError;

    private Material materialMainCandado;

    private bool bOnMouseOverFirstTime = true;

    private Animator animDevice;
    private bool bUsing = false;
    private int isOpenHash;

    public float getCost()
    {
        return fMoneyModifier;
    }

    private void OnMouseOver()
    {
        //        labelDevice.enabled = true;
        Cursor.SetCursor(textureCursorOver,Vector2.zero,CursorMode.Auto);

        if (bOnMouseOverFirstTime == true)
        {
            audioSoundMouseOver.Play();
            bOnMouseOverFirstTime = false;
        }
        labelDevice.SetActive(true);
    }

    private void OnMouseExit()
    {
        //        labelDevice.enabled = false;
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        bOnMouseOverFirstTime = true;
        labelDevice.SetActive(false);
    }

    void OnMouseDown()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        if (bUsing == false)
        {
            bUsing = true;

            myAnalytics.increaseTimeUsed();

            myTV.setTVOff();
            myTV.hideChannelsImage();


            //        myCharacterManager.setDeviceActive(iIndex, false);
            if(myCharacterManager.getIsTVAllowed() == false)
            {
                mySceneManager.showText(Localization.Get("CantUse"));
                materialLuzCandado.material = materialRojoCandado;
                soundError.Play();
                StartCoroutine(closeDevice(1));
            }
            else if (myCharacterManager.getEnoughMoney(fMoneyModifier) == true)
            {
                myCharacterManager.restaMoney(fMoneyModifier);
                materialLuzCandado.material = materialVerdeCandado;
                soundCoin.Play();

                myCharacterManager.receiveModifiers(aValueModifier, iSeconds);
                myCharacterManager.sumaPoliticalAffinity(fPoliticalModifier);

                soundUse.Play();
                animDevice.SetBool(isOpenHash, true);
                StartCoroutine(closeDevice(3));
            }
            else
            {
//                mySceneManager.showText("No tienes suficiente dinero");
                mySceneManager.showText(Localization.Get("NoMoney"));
                materialLuzCandado.material = materialRojoCandado;
                soundError.Play();
                StartCoroutine(closeDevice(2));
            }
            //            myDystopicCharacterController.setNewState(dystopicCharacterController.characterState.IDLE);
        }
    }

/*
    IEnumerator showHideText()
    {
        //TODO: texto para traducir
        mySceneManager.setTextMessages("No tienes suficiente dinero"); ;
        yield return new WaitForSeconds(3);
        mySceneManager.setTextMessages(""); ;
    }
*/
    IEnumerator closeDevice(int iSeconds)
    {
        yield return new WaitForSeconds(iSeconds);
        bUsing = false;
        animDevice.SetBool(isOpenHash, false);
        materialLuzCandado.material = materialMainCandado;
        soundUse.Stop();
        soundCoin.Stop();
    }



    private void Awake()
    {
        animDevice = this.GetComponent<Animator>();
        isOpenHash = Animator.StringToHash("openDevice");

        materialMainCandado = materialLuzCandado.material;

        //        labelDevice.enabled = false;
        labelDevice.SetActive(false);
    }
}
