﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lights_Behaviour : MonoBehaviour {
    public int iMinSecondsToBlink = 3;
    public int iMaxSecondsToBlink = 3;

    private Animator animDevice;
    private int isOpenHash;

    public void StartBlink()
    {
        animDevice.SetBool(isOpenHash, false);
        StartCoroutine(CorBlink());
    }

    IEnumerator CorBlink()
    {
        yield return new WaitForSeconds(Random.Range(iMinSecondsToBlink, iMaxSecondsToBlink));
        animDevice.SetBool(isOpenHash, true);
        StartCoroutine(CorIdle());
    }

    IEnumerator CorIdle()
    {
        yield return new WaitForSeconds(2);
        StartBlink();
    }

    void Start()
    {
        animDevice = this.GetComponent<Animator>();
        isOpenHash = Animator.StringToHash("openDevice");
        StartBlink();
    }
}
