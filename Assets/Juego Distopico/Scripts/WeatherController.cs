﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherController : MonoBehaviour {

    public int iSecondsToWait = 30;
    public int iMinSeconds = 20;
    public int iMaxSeconds = 60;
    public GameObject[] arrayGoAnimated;


    private enum eWeatherState
    {
        SUN,
        STORM,
        WIND
    }

    private eWeatherState myWeather;


    private void DisableAllSounds()
    {
        for (int i = 0; i < arrayGoAnimated.Length; i++)
        {
            arrayGoAnimated[i].SetActive(false);
        }
    }

    private void SelectNewWeather()
    {
        int iTempValue = Random.Range(0, 4);
        switch (iTempValue)
        {
            case ((int)eWeatherState.SUN):
                Debug.Log("EL tiempo es SOLEADO ");
                arrayGoAnimated[(int)eWeatherState.SUN].SetActive(true);
                break;
            case ((int)eWeatherState.STORM):
                Debug.Log("EL tiempo es LLUVIA");
                arrayGoAnimated[(int)eWeatherState.STORM].SetActive(true);
                break;
            case ((int)eWeatherState.WIND):
                Debug.Log("EL tiempo es VENTOSO ");
                arrayGoAnimated[(int)eWeatherState.WIND].SetActive(true);
                break;
            default:
                break;
        }

        StartCoroutine(corStopWeather());
    }

    IEnumerator corStopWeather()
    {
        yield return new WaitForSeconds(iSecondsToWait);
        DisableAllSounds();
        StartCoroutine(corNewWeather());
    }
       
    IEnumerator corNewWeather()
    {
        int iRandomSeconds = Random.Range(iMinSeconds, iMaxSeconds);
        yield return new WaitForSeconds(iRandomSeconds);
        SelectNewWeather();
    }

    void Start()
    {
        DisableAllSounds();
        StartCoroutine(corNewWeather());
    }
}
