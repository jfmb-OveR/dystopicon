﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class dystopicGameManager : MonoBehaviour {

    public static dystopicGameManager myGameManagerInstance;
    public GameObject goFadeIn;
    public GameObject goFadeOut;
    public characterManager myCharacterManager;
    public dystopicSceneManager myDystopicSceneManager;
    public newsController myNewsController;
    public Clock myClock;
    public GameObject[] mySceneObjects;
    public bool[] myActiveObjects;
    [Header("Objetos de las hidden stories")]
    public GameObject[] aGoHiddenStories;

    public GameObject goImageFinalNews;
    public Text textNews;
    [Header("El controlador de las ratas")]
    public RatsController myRatsController;
    public GameObject goMainTheme;
    [Header("Canvas de stats")]
    public GameObject goCanvasStats;
    [Header("Canvas de etiquetas")]
    public GameObject goCanvasLabels;
    public useEuthanasiaMachine exitWithEscape;

    private BoxCollider[] arrayColliders;
    private float fPoliticalAffinityFinalValue;
    private float fTotalMoneyFinalValue;
    private int iTotalDaysPlayed;

    private bool bGamePaused = false;

    public enum EnumTypeOfEnd
    {
        PLAYER_DIED = 0, //El jugador ha muerto
        EUTHANASIA = 1,  //El jugador ha terminado usando la máquina de eutanasia
        WITH_HIGH_AFFINITY = 2, //Termina el periodo de pruebas y el jugador tiene una afinidad política alta
        WITH_MED_AFFINITY = 3, //El jugador ha terminado el periodo de pruebas con una afinidad política media
        WITHOUT_AFFINITY = 4, //Termina el periodo de pruebas pero no tiene afinidad política
        LOTTERY = 5, //El juador ha ganado la lotería y un trabajo permanente
        PAY_500 = 6, //El jugador ha pagado 500 euros para terminar
        DISIDENT = 7 //El jugador ha realizado el atentado y se ha pasado a los disidentes. 
    }

#region Getters_Setters
    public bool GetGamePaused() {
        return bGamePaused;
    }

    public float getPoliticalAffinityFinalValue()
    {
        return fPoliticalAffinityFinalValue;
    }

    public void setPoliticalAffinityFinalValue()
    {
        fPoliticalAffinityFinalValue  = myCharacterManager.getPoliticalAffinity();
    }

    public void setTotalMoneyFinalValue()
    {
        fTotalMoneyFinalValue = myCharacterManager.getTotalMoney();
    }

    public void setTotalDaysPlayed()
    {
        iTotalDaysPlayed = myDystopicSceneManager.GetCurrentNumberOfDays();
    }
#endregion
    public void EndGame(int iTypeOfEnd)
    {
        goFadeOut.SetActive(true);
        StartCoroutine(CorEndGame(iTypeOfEnd));
    }

    public void ShowStatsBeforeFadeOut(int iTypeOfEnd)
    {
        string sEnding ="";

        myNewsController.ActivateFinalNewsButtonFromOutside();

        setPoliticalAffinityFinalValue();
        setTotalMoneyFinalValue();
        setTotalDaysPlayed();

        goMainTheme.SetActive(true);

        switch (iTypeOfEnd)
        {
            case ((int)EnumTypeOfEnd.PLAYER_DIED):
                textNews.text = Localization.Get("FinalMessage00") + "\n\n" + Localization.Get("TotalDaysPlayed") + iTotalDaysPlayed +"\n" + Localization.Get("PoliticalAffinity") + fPoliticalAffinityFinalValue + "\n" + Localization.Get("TotalMoney") + fTotalMoneyFinalValue.ToString("0.00") + " $";
                Debug.Log("ººººººººººººººººººººººº El juego ha terminado porque el jugador ha muerto");
                sEnding = "Ending_Player_Dead";
                break;
            case ((int)EnumTypeOfEnd.EUTHANASIA):
                Debug.Log("ººººººººººººººººººººººº El juego ha terminado porque el jugador ha hecho eutanasia");
                textNews.text = Localization.Get("FinalMessage01") + "\n\n" + Localization.Get("TotalDaysPlayed") + iTotalDaysPlayed + "\n" + Localization.Get("PoliticalAffinity") + fPoliticalAffinityFinalValue + "\n" + Localization.Get("TotalMoney") + fTotalMoneyFinalValue.ToString("0.00") + " $";
                sEnding = "Ending_Euthanasia";
                break;
            case ((int)EnumTypeOfEnd.WITH_HIGH_AFFINITY):
                Debug.Log("ººººººººººººººººººººººº El juego ha terminado con HIGH AFFINITY");
                textNews.text = Localization.Get("FinalMessage02") + "\n\n" + Localization.Get("TotalDaysPlayed") + iTotalDaysPlayed + "\n" + Localization.Get("PoliticalAffinity") + fPoliticalAffinityFinalValue + "\n" + Localization.Get("TotalMoney") + fTotalMoneyFinalValue.ToString("0.00") + " $";
                sEnding = "Ending_High_Affinity";
                break;
            case ((int)EnumTypeOfEnd.WITH_MED_AFFINITY):
                Debug.Log("ººººººººººººººººººººººº El juego ha terminado CON MED AFFINITY");
                textNews.text = Localization.Get("FinalMessage03") + "\n\n" + Localization.Get("TotalDaysPlayed") + iTotalDaysPlayed + "\n" + Localization.Get("PoliticalAffinity") + fPoliticalAffinityFinalValue + "\n" + Localization.Get("TotalMoney") + fTotalMoneyFinalValue.ToString("0.00") + " $";
                sEnding = "Ending_Med_Affinity";
                break;
            case ((int)EnumTypeOfEnd.WITHOUT_AFFINITY):
                Debug.Log("ººººººººººººººººººººººº El juego ha terminado SIN AFFINITY");
                textNews.text = Localization.Get("FinalMessage04") + "\n\n" + Localization.Get("TotalDaysPlayed") + iTotalDaysPlayed + "\n" + Localization.Get("PoliticalAffinity") + fPoliticalAffinityFinalValue + "\n" + Localization.Get("TotalMoney") + fTotalMoneyFinalValue.ToString("0.00") + " $";
                sEnding = "Ending_Low_Affinity";
                break;
            case ((int)EnumTypeOfEnd.LOTTERY):
                Debug.Log("ººººººººººººººººººººººº El juego ha terminado porque el jugador ha ganado la lotería");
                textNews.text = Localization.Get("FinalMessage05") + "\n\n" + Localization.Get("TotalDaysPlayed") + iTotalDaysPlayed + "\n" + Localization.Get("PoliticalAffinity") + fPoliticalAffinityFinalValue + "\n" + Localization.Get("TotalMoney") + fTotalMoneyFinalValue.ToString("0.00") + " $";
                sEnding = "Ending_Lottery";
                break;
            case ((int)EnumTypeOfEnd.PAY_500):
                Debug.Log("ººººººººººººººººººººººº El juego ha terminado porque el jugador ha pagado 500 $");
                textNews.text = Localization.Get("FinalMessage06") + "\n\n" + Localization.Get("TotalDaysPlayed") + iTotalDaysPlayed + "\n" + Localization.Get("PoliticalAffinity") + fPoliticalAffinityFinalValue + "\n" + Localization.Get("TotalMoney") + fTotalMoneyFinalValue.ToString("0.00") + " $";
                sEnding = "Ending_Player_paid_500";
                break;
            case ((int)EnumTypeOfEnd.DISIDENT):
                Debug.Log("ººººººººººººººººººººººº El juego ha terminado porque el jugador es un disidente");
                textNews.text = textNews.text + "\n\n" + Localization.Get("PackageTaken");
                textNews.text = Localization.Get("FinalMessage07") + "\n\n" + Localization.Get("TotalDaysPlayed") + iTotalDaysPlayed + "\n" + Localization.Get("PoliticalAffinity") + fPoliticalAffinityFinalValue + "\n" + Localization.Get("TotalMoney") + fTotalMoneyFinalValue.ToString("0.00") + " $";
                sEnding = "Ending_Player_Disident";
                break;
        }

        goImageFinalNews.SetActive(true);
        Analytics.CustomEvent("Endings_DaysPlayed", new Dictionary<string, object>
        {
            {sEnding, iTotalDaysPlayed }
        });
        GetAnalytics();

//       StartCoroutine(CorWaitToExit());
    }

    private void GetAnalytics()
    {
        for (int i = 0; i < mySceneObjects.Length; i++)
        {
            if(mySceneObjects[i].GetComponent<myAnalyticsScriptForDevices>() != null)
                mySceneObjects[i].GetComponent<myAnalyticsScriptForDevices>().setAnalytics();
        }
    }

//    private void ActivateDeactivateColliders(bool bNewValue)

    public void ActivateColliders(bool bNewValue){
        for (int i = 0; i < arrayColliders.Length; i++)
        {
            arrayColliders[i].enabled = bNewValue;
        }
        if (bNewValue == true)//Hay que activar los colliders de las Hidden Stories
        {
            for (int i = 0; i < aGoHiddenStories.Length-1; i++)
            {
                aGoHiddenStories[i].GetComponent<HiddenStoryScript>().CheckBeforeActivate();
            }
            aGoHiddenStories[5].GetComponent<ArmarioScript>().CheckBeforeActivate();
        }
        else //Hay que desactivarlos
        {
            for (int i = 0; i < aGoHiddenStories.Length; i++)
            {
                aGoHiddenStories[i].GetComponent<BoxCollider>().enabled = false;
            }
        }
    }
/*
    IEnumerator ActivateDeactivateColliders(bool bNewValue)
    {
        for (int i = 0; i < arrayColliders.Length; i++)
        {
            arrayColliders[i].enabled = bNewValue;
        }
        if (bNewValue == true)//Hay que activar los colliders de las Hidden Stories
        {
            for (int i = 0; i < aGoHiddenStories.Length-1; i++)
            {
                aGoHiddenStories[i].GetComponent<HiddenStoryScript>().CheckBeforeActivate();
            }
            aGoHiddenStories[5].GetComponent<ArmarioScript>().CheckBeforeActivate();
        }
        else //Hay que desactivarlos
        {
            for (int i = 0; i < aGoHiddenStories.Length; i++)
            {
                aGoHiddenStories[i].GetComponent<BoxCollider>().enabled = false;
            }
        }
        yield return null;
    }
*/
    public void ActivateDeactivateCollidersExceptTV(bool bNewValue)
    {
        for (int i = 2; i < arrayColliders.Length; i++)
        {
            switch (i)
            {
                case (13):
                    arrayColliders[i].enabled = true;
                    break;
                case (16):
                    arrayColliders[i].enabled = true;
                    break;
                case (17):
                    arrayColliders[i].enabled = true;
                    break;
                default:
                    arrayColliders[i].enabled = bNewValue;
                    break;
            }
//            Debug.Log("activando el objeto " + i);
        }
    }

    public void ActivateObject(int idObject)
    {
        mySceneObjects[idObject].SetActive(true);
        myActiveObjects[idObject] = true;
        arrayColliders[idObject].enabled = true;
    }

    public void PauseGame()
    {
        goCanvasStats.SetActive(false);
        goCanvasLabels.SetActive(false);
        ActivateColliders(false);
        myRatsController.AllRatsActivated(false);
        myClock.SetPointerSound(false);
        bGamePaused = true;
        Debug.Log("He pausado el juego");
    }

    public void ResumeGame()
    {
        ActivateColliders(true);
        myRatsController.AllRatsActivated(true);
        bGamePaused = false;
        Debug.Log("He reanudado el juego");
        myClock.SetPointerSound(true);
        goCanvasStats.SetActive(true);
        goCanvasLabels.SetActive(true);
    }

    public void ExitGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    IEnumerator CorEndGame(int iTypeOfEnd){
        yield return new WaitForSeconds(1.5f);
        ShowStatsBeforeFadeOut(iTypeOfEnd);
    }

/*
    IEnumerator CorPauseGame()
    {
        yield return new WaitForSeconds(0.5f);
        Debug.Log("He pausado el tiempo");
        Time.timeScale = 0f;
    }
*/
/*
    IEnumerator CorPauseGame()
    {
        //yield return StartCoroutine(ActivateDeactivateColliders(false));
        yield return StartCoroutine(myRatsController.AllRatsActivated(false));
        Debug.Log("He pausado el tiempo");
//        Time.timeScale = 0f;
        bGamePaused = true;
    }
    IEnumerator CorResumeGame()
    {
        yield return StartCoroutine(ActivateDeactivateColliders(true));
        yield return StartCoroutine(myRatsController.AllRatsActivated(true));
//        Time.timeScale = 1f;
        bGamePaused = false;
        Debug.Log("He reanudado el tiempo");
        goCanvasStats.SetActive(true);
        goCanvasLabels.SetActive(true);
    }
*/
/*
    IEnumerator CorWaitToExit()
    {
        yield return new WaitForSeconds(1f);
        ExitGame();
    }
*/

    public void LocalizeTexts(){
        goImageFinalNews.transform.GetChild(2).gameObject.GetComponent<Text>().text = Localization.Get("ButtonEnd");
    }

    void Awake()
    {
        myGameManagerInstance = this;

        goMainTheme.SetActive(false);

        goFadeIn.SetActive(false);
        goFadeOut.SetActive(false);
    }

    void Start()
    {
        arrayColliders = new BoxCollider[mySceneObjects.Length];
        for (int i = 0; i < mySceneObjects.Length; i++)
        {
            arrayColliders[i] = mySceneObjects[i].GetComponent<BoxCollider>();
            mySceneObjects[i].SetActive(myActiveObjects[i]);
        }

        LocalizeTexts();
        goFadeIn.SetActive(true);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) == true)
        {
            //            ExitGame();
            exitWithEscape.OnMouseDown();
        }
    }
}
