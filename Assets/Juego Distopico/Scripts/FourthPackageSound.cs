﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourthPackageSound : MonoBehaviour {
    public GameObject goAudioStreetParade;	// Use this for initialization
    public newsController myNewsController;
    public int iSecondsToStop = 30;

    void Awake () {
        goAudioStreetParade.SetActive(true);
        StartCoroutine(CorStopAudio());
        myNewsController.StopAllComicSounds();
    }

    IEnumerator CorStopAudio()
    {
        yield return new WaitForSeconds(iSecondsToStop);
        goAudioStreetParade.SetActive(false);
    }
}
