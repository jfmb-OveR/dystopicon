﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class lotteryScript : MonoBehaviour {

    public dystopicSceneManager mySceneManager;
    public characterManager myCharacterManager;
    public GameObject goLotteryMessage;
    public float fOneNumberPrize = 1;
    public float fTwoNumbersPrize = 2;
    public float fThreeNumbersPrize = 10;
    public float fFourNumbersPrize = 100;
    public float fFiveNumbersPrize = 0;

    private bool bUsingMachine = false;
    private string sMessage = "";

    private void OnMouseDown()
    {
        if(bUsingMachine == false)
        {
            bUsingMachine = true;

            int iCounter = 0;
            //        char[] sFirstNummber = new char[5];
            //        char[] sWinnerNummber = new char[5];

            string sFirstNumber = "";
            string sWinnerNumber = "";

            for (int i = 0; i < 5; i++)
            {
                sFirstNumber += Random.Range(0, 9).ToString();
                sWinnerNumber += Random.Range(0, 9).ToString();

                if (sFirstNumber[i] == sWinnerNumber[i])
                    iCounter++;
            }

            switch (iCounter)
            {
                case 0:
//                    mySceneManager.showText(Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage02"));
                    sMessage = (Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage02"));
                    break;
                case 1:
                    myCharacterManager.setMoreMoney(fOneNumberPrize);
                    //                    mySceneManager.showText(Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage00_win01"));
                    sMessage = (Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage00_win01"));
                    break;
                case 2:
                    myCharacterManager.setMoreMoney(fTwoNumbersPrize);
                    //                    mySceneManager.showText(Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage00_win01"));
                    sMessage = (Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage00_win01"));
                    break;
                case 3:
                    myCharacterManager.setMoreMoney(fThreeNumbersPrize);
                    //                    mySceneManager.showText(Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage00_win03"));
                    sMessage = (Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage00_win03"));
                    break;
                case 4:
                    myCharacterManager.setMoreMoney(fFourNumbersPrize);
                    //                  mySceneManager.showText(Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage00_win04"));
                    sMessage = (Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage00_win04"));
                    break;
                case 5: //Gana un trabajo!
                    //                    mySceneManager.showText(Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage00_win05"));
                    sMessage = (Localization.Get("LotteryMessage00") + sFirstNumber + "\n" + Localization.Get("LotteryMessage01") + sWinnerNumber + "\n" + Localization.Get("LotteryMessage00_win05"));
                    goLotteryMessage.GetComponent<Text>().text = sMessage;
                    goLotteryMessage.SetActive(true);
                    StartCoroutine(CorEndGameLottery());
                    break;
            }
            StartCoroutine(AllowUseMachineAgain(sMessage));
        }
    }

    IEnumerator AllowUseMachineAgain(string sText)
    {
        goLotteryMessage.GetComponent<Text>().text = sText;
        goLotteryMessage.SetActive(true);
        yield return new WaitForSeconds(3);
        goLotteryMessage.SetActive(false);
        goLotteryMessage.GetComponent<Text>().text = "";
        bUsingMachine = false;
    }

    IEnumerator CorEndGameLottery()
    {
        yield return new WaitForSeconds(3);
        dystopicGameManager.myGameManagerInstance.EndGame((int)dystopicGameManager.EnumTypeOfEnd.LOTTERY);
    }
}
