﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class myAnalyticsScriptForDevices : MonoBehaviour {

    public string sDeviceName;

    private int iTimesUsed = 0;

    public void increaseTimeUsed()
    {
        iTimesUsed++;
    }

    public void setAnalytics()
    {
        Debug.Log("Actualizando Analytics de " + sDeviceName);
        Analytics.CustomEvent("Use_Device", new Dictionary<string, object>
        {
            {sDeviceName, iTimesUsed }
        });
    }
}
