﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class channelScript : MonoBehaviour {
    public float fMoneyPaid;
    public float fFunModifier;
    public float fPoliticalModifier;
    public useTV_v2 myTV;
    public AudioSource soundWhiteNoise;

    public Text  textChannel;

    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;

    //    public bool bChannelSelected;

    public int bChannelNumber = 1;

    private void SetGray()
    {
        textChannel.color = Color.gray;

    }

    private void SetBlack()
    {
        textChannel.color = Color.black;
    }

    private void OnMouseOver()
    {
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);

        SetGray();
    }
    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        SetBlack();
    }

    private void OnMouseDown()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        changeChannel();
        SetBlack();
    }

    public void changeChannel()
    {
        switch (bChannelNumber)
        {
            case 1:
                myTV.SetColorRed(); //Politics
                break;
            case 2:
                myTV.SetColorYellow(); //Religion
                break;
            case 3:
                myTV.SetColorBlue(); //Entertaiment
                break;
            case 4:
                myTV.SetColorBlack(); //News
                break;
            case 5:
                myTV.SetColorGreen(); //sPORTS
                break;
            case 6:
                myTV.SetColorMagenta(); //pORN
                break;
        }
        soundWhiteNoise.Play();

        myTV.setMoneyModifier(fMoneyPaid);
        myTV.setFunValueModifier(fFunModifier);
        myTV.setPoliticalModifier(fPoliticalModifier);
        myTV.showHideChannelsImage();
        myTV.setTVOn();
    }

    void Start()
    {
        switch (bChannelNumber)
        {
            case 1:
                textChannel.text = Localization.Get("PoliticalChannel");
                break;
            case 2:
                textChannel.text = Localization.Get("ReligionChannel");
                break;
            case 3:
                textChannel.text = Localization.Get("EntertainmentChannel");
                break;
            case 4:
                textChannel.text = Localization.Get("NewsChannel");
                break;
            case 5:
                textChannel.text = Localization.Get("SportsChannel");
                break;
            case 6:
                textChannel.text = Localization.Get("PornChannel");
                break;

        }
    }
}
