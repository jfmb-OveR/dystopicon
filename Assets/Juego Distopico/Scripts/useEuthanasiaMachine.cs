﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class useEuthanasiaMachine : MonoBehaviour {
    //    public dystopicSceneManager mySceneManager;
    //    public characterManager myCharacterManager;
    public myAnalyticsScriptForDevices myAnalytics;

    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;

    public GameObject goImageConfirmation;
    public AudioSource audioOnMouseOver;
    public useTV_v2 myTV;

    public UILabel labelDevice;

    private bool bOnMouseOverFirstTime = true;

    private void OnMouseOver()
    {
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);

        //        textDevice.enabled = true;
        if (bOnMouseOverFirstTime == true)
        {
            audioOnMouseOver.Play();
            bOnMouseOverFirstTime = false;
        }
        labelDevice.enabled = true;
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        //        textDevice.enabled = false;
        bOnMouseOverFirstTime = true;
        labelDevice.enabled = false;
    }

    public void OnMouseDown()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        myAnalytics.increaseTimeUsed();

        myTV.setTVOff();
        myTV.hideChannelsImage();
        StartCoroutine(showMessage());
//        goImageConfirmation.SetActive(true);
    }

    public void ConfirmationYes()
    {
        dystopicGameManager.myGameManagerInstance.ResumeGame();
//        dystopicGameManager.myGameManagerInstance.setPoliticalAffinityFinalValue();
        dystopicGameManager.myGameManagerInstance.EndGame((int)dystopicGameManager.EnumTypeOfEnd.EUTHANASIA);
    }

    public void ConfirmationNo()
    {
        goImageConfirmation.SetActive(false);
        dystopicGameManager.myGameManagerInstance.ResumeGame();
    }

    IEnumerator showMessage()
    {
        yield return new WaitForSeconds(0.05f);
        dystopicGameManager.myGameManagerInstance.PauseGame();
        //        dystopicGameManager.myGameManagerInstance.PauseGame();
        goImageConfirmation.SetActive(true);
    }


    private void Awake()
    {
        goImageConfirmation.SetActive(false);
        labelDevice.enabled = false;
    }

}
