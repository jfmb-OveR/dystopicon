﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class policeLights : MonoBehaviour {
    public Transform transformBlue;
    public Transform transformRed;
    public float fCycleSpeed = 10f;

    private bool bRotate = false;
	// Use this for initialization

    public void setRotate(bool bNewValue)
    {
        bRotate = bNewValue;
    }
	
	// Update is called once per frame
	void Update () {
//		if(bRotate == true)
        {
            transformBlue.Rotate(Vector3.down * fCycleSpeed);
            transformRed.Rotate(Vector3.down * fCycleSpeed);
        }
    }
}
