﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myLocalizationController : MonoBehaviour {
    [SerializeField]
    private MenuController myMenuController;

    public void setEnglish()
    {
        Localization.language = "English";
        myMenuController.ShowHideLanguageSelector();
    }

    public void setSpanish()
    {
        Localization.language = "Spanish";
        myMenuController.ShowHideLanguageSelector();
    }
}
