﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class changeShaderInObject : MonoBehaviour {
    public Renderer myRend01;
    public Shader shaderOriginal;
    public Shader shaderOnMousseOver;

    void OnMouseOver()
    {
        myRend01.material.shader = shaderOnMousseOver;
    }

    void OnMouseExit()
    {
        myRend01.material.shader = shaderOriginal;
    }

    // Use this for initialization
    void Start () {
        //        myRend = this.GetComponent<Renderer>();
        myRend01.material.shader = shaderOriginal;
    }

}
