﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class dystopicCharacterController : MonoBehaviour {
    /*
    public float fInputDelay = 0.1f;
    public float fRunningThreshold = 0.5f;
    public float fWalkSpeed = 3f;
    public float fRunSpeed = 5f;
    public float fTurnSpeed = 3f;

    public Transform tTarget;
    public float fDist;

    private Vector3 vDestination;
    private NavMeshAgent nmAgent;
*/

//    public Material materialCharacter;
//    private Color colorCharacter;

    private bool bIsWatchingTV;

    [HideInInspector]
    public enum characterState
    {
        IDLE,
        WALKING, 
        WATCHING_TV
    }

    private characterState state;

    private Animator characterAnimator;
    private int isWatchingTVHash;
//    private int isWalkingHash;

    //    private Rigidbody rbCharacter;
    //    private Transform tCharacter;
    //    private Vector3 vDirection = new Vector3();
    //    private Vector3 vDirectionY = new Vector3();
    //    private Quaternion qCharacterRotation;

    //    private bool bIsWalking = false;

    private float fTemp = 0f;

    #region
    public bool getWatchingTV()
    {
        return bIsWatchingTV;
    }
    #endregion

    #region setters
    public void SetPosition(Transform newPosition)
    {
        this.gameObject.transform.position = newPosition.position;
        this.gameObject.transform.rotation = newPosition.rotation;
    }

/*
    public void setDestination(Vector3 newDestination)
    {
        vDestination = newDestination;
//        setNewState(dystopicCharacterController.characterState.WALKING);
    }
*/
    #endregion
/*
    void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "obstacle")
        {
//            setNewState(characterState.IDLE);
//            rbCharacter.MovePosition(tCharacter.position + Vector3.zero * fRunSpeed * Time.deltaTime);

            Debug.Log("Colisión!!!");
        }
    }

    public Quaternion getRotation()
    {
        return tCharacter.rotation;
    }
    */
    public void setIdleState()
    {
        setNewState(characterState.IDLE);
    }

    public void setWatchingTVState()
    {
        setNewState(characterState.WATCHING_TV);
    }

    public void setWalkingState()
    {
        setNewState(characterState.WALKING);
    }

    public void setNewState(characterState newState)
    {
        //        float fThreshold = 0f;
        if (state != newState)
        {
            state = newState;
            switch (state)
            {
                case characterState.IDLE:
//                    bIsWalking = false;

                    bIsWatchingTV = false;
                    fTemp = 0f;
                    break;
                case characterState.WATCHING_TV:
//                    bIsWalking = false;
                    bIsWatchingTV = true;
                    break;
                case characterState.WALKING:
//                    bIsWalking = true;
                    bIsWatchingTV = false;
//                    isWalking();
                    break;
            }
        }
//        characterAnimator.SetBool(isWatchingTVHash, bIsWatchingTV);
//        characterAnimator.SetBool(isWalkingHash, bIsWalking);
    }

    /*
    public void isIdle()
    {
    }

    public void isWalking()
    {
    }

    public void turnCharacter()
    {
        //        Vector3 newDirection = new Vector3(fHorinzotalInput, 0,fVerticalInput);
        vDirection = this.transform.position - vDestination;

        if (vDirection != Vector3.zero)
            tCharacter.rotation = Quaternion.RotateTowards(tCharacter.rotation, Quaternion.LookRotation(vDirection), Time.time * fTurnSpeed);

        //        qCharacterRotation = Quaternion.FromToRotation(tCharacter.forward, vDirectionX + vDirectionY);
        //        tCharacter.rotation = Quaternion.Lerp(tCharacter.rotation, qCharacterRotation, fTurnSpeed * Time.deltaTime);
    }
*/



    void Awake()
    {

        characterAnimator = this.GetComponent<Animator>();
        isWatchingTVHash = Animator.StringToHash("isWatchingTV");

        //colorCharacter = materialCharacter.color;

        //        materialCharacter.color = new Color(colorCharacter.r, colorCharacter.g, colorCharacter.b, 255f);
        setWatchingTVState();

//        isWalkingHash = Animator.StringToHash("isWalking");

//        rbCharacter = this.GetComponent<Rigidbody>();
//        tCharacter = this.GetComponent<Transform>();
//        setIdleState();

//        nmAgent = this.GetComponent<NavMeshAgent>();
    }

    void Update()
    {
/*        if (bIsWatchingTV == false)
        {
            if (colorCharacter.a > 0)
                materialCharacter.color = new Color(colorCharacter.r, colorCharacter.g, colorCharacter.b, colorCharacter.a - (20f * Time.deltaTime));

            Debug.Log("He entrado en el update!!!!!!!!!!!!!!!!!!!!!!!");
        }
        */
    }

    /*
        // Update is called once per frame
        void Update()
        {
            if (bIsWalking == true)
            {
    //            turnCharacter();
                if (Vector3.Distance(vDestination, tTarget.position) > fDist)
                {
    //                vDestination = tTarget.position;
                    nmAgent.destination = vDestination;
                }
                else
                {
                    setNewState(characterState.IDLE);
                }
            }
    //        else
    //            Debug.Log("eSTOY PARADO!!!");

        }

        void FixedUpdate()
        {
    //        moveCharacter();
        }
        */
}
