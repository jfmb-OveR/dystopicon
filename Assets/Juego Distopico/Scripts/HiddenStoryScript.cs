﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenStoryScript : MonoBehaviour {
    public HiddenStoriesController myStoryController;
    public GameObject goStar;
    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;


    public string sObjecto;

    private bool bIsActive = false;

    
    public void CheckBeforeActivate()
    {
        if (bIsActive == true)
            SetActiveCollider();
    }

    public void SetActiveCollider()
    {
        this.gameObject.GetComponent<BoxCollider>().enabled = true;
    }


    void OnMouseDown()
    {
        myStoryController.ShowNewStory();
        bIsActive = false;
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
        goStar.SetActive(false);
    }

    private void OnMouseOver()
    {
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);
    }

    public void ActivateStar()
    {
        //this.gameObject.GetComponent<BoxCollider>().enabled = true;
        bIsActive = true;
        SetActiveCollider();
        StartCoroutine(ActivateStarCoroutine());
    }

    IEnumerator ActivateStarCoroutine()
    {
        yield return new WaitForSeconds(Random.Range(1,5));
        goStar.SetActive(true);
    }



    void Awake()
    {
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
        goStar.SetActive(false);
    }


}
