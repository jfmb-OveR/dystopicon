﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dystopicSceneManager : MonoBehaviour {
    [SerializeField]
    private float fDaySpeed;

    public Texture2D textureCursorNormal;

    //    public float fDayDuration;

    public Image imageHealth;
    public Image imageHungry;
    public Image imageComfort;
    public Image imageFun;
    public Image imagePoliticalAffinity;

    public GameObject[] goWeirdPackage;

    public newsPaperScript myNewspaperScript;
    public newsController myNewsController;
    public DayNightScript myDayNightScript;

    public Text textHealth;
    public Text textHunger;
    public Text textComfort;
    public Text textFun;
    public Text textMoney;
    public Text textMessages;
    public characterManager myCharacterManager;

    public Text textUPHealth;
    public Text textUPHunger;
    public Text textUPComfort;
    public Text textUPFun;
    public Text textUPMoney;

    public Text textDOWNHealth;
    public Text textDOWNHunger;
    public Text textDOWNComfort;
    public Text textDOWNFun;
    public Text textDOWNMoney;

    [Header("Días totales del periodo de pruebas")]
    public int iTotalDays;
    private float fDayDuration;

    private int iDays = 0;
    private bool bBlockDaysCounter = false;

    int iDayForNewsController = 0;
    //    private bool bDawn = false;
    private int iDayOfThePackage; //Día semilla en el que aparece el paquete
    private int iRandomDayOfTheFirstPackage; //Día semilla en el que aparece el paquete
    private int iRandomDayOfTheSecondPackage; //Día segundo paquete;
    private int iRandomDayOfThThirdPackage; //Día segundo paquete;
    private int iRandomDayOfTheFourthPackage; //Día segundo paquete;

    private Color32 colorCustom = new Color32(180,175,175,255);

    private string sHealth;
    private string sHunger;
    private string sComfort;
    private string sFun;
    private string sMoney;
    private string sDown;
    private string sUp;

    private float fHealthANT;
    private float fHungertANT;
    private float fComfortANT;
    private float fFunANT;
    private float fMoneyANT;

    private float fHealthCURRENT;
    private float fHungertCURRENT;
    private float fComfortCURRENT;
    private float fFunCURRENT;
    private float fMoneyCURRENT;
    private float fPoliticalAffinityCURRENT;

    private bool[] arraySpecialEvents = new bool[4]; //Guarda eventos especiales: 0: primer paquete, 1: segundo paquete, 2: tercer paquete, 3: evento asesinato

    private enum SpecialEvents
    {
        Paquete_Primero,
        Paquete_Segundo,
        Paquete_Tercero,
        Evento_Asesinato
    }

    #region getters
    public float GetDayVelocity()
    {
        return fDaySpeed;
    }

    public bool GetWindowHasEventToday()
    {
        return myNewsController.getWindowHasEvent();
    }

    public bool GetSpecialEvent(int iIndex)
    {
        return arraySpecialEvents[iIndex];
    }

    public int GetCurrentNumberOfDays()
    {
        return iDays;
    }
    #endregion

    #region setters
    public void setTextMessages(string newString)
    {
        textMessages.text = newString;
    }

    public void setSpecialEvent(int iIndex, bool bValue)
    {
        arraySpecialEvents[iIndex] = bValue;
    }

    #endregion

    public void showText(string newString)
    {
        StartCoroutine(showHideText(newString));
    }

    public void showTextWithNews(string sNews)
    {
        setTextMessages(sNews);
    }
/*
    public string getNewsFromDay()
    {
        return myNewsController.getNewsFromDay(iDayForNewsController);
    }
    */
    public void showNewsFromToday()
    {
        myNewsController.showNews();
    }

    public void showWindowEventsFromToday()
    {
        showText(Localization.Get("Window") + myNewsController.getEventFromWindow());
        myCharacterManager.sumaPoliticalAffinity(myNewsController.getPoliticalAffinityCurrentComic());
    }

    public void CheckSpecialEvent() //comprueba si ese día hay paquete
    {
        //El primer paquete llega por debajo de la puerta

        if (iDays == iDayOfThePackage + iRandomDayOfTheFirstPackage)
        {
            goWeirdPackage[(int)SpecialEvents.Paquete_Primero].SetActive(true);
        }

        else if (iDays == iDayOfThePackage + iRandomDayOfTheSecondPackage)
        {
            if (GetSpecialEvent((int)SpecialEvents.Paquete_Primero) == true)
            {
                //El primer paquete fue aceptado y llega el segundo paquete con los planes
                goWeirdPackage[(int)SpecialEvents.Paquete_Segundo].SetActive(true);
//                Debug.Log("Segundo paquete!!!!!!!");
            }
            else
            {
                goWeirdPackage[(int)SpecialEvents.Paquete_Primero].GetComponent<weirdPackageClicked>().DesactivateLabel();
                goWeirdPackage[(int)SpecialEvents.Paquete_Primero].SetActive(false);
            }
        }

        if (iDays == iDayOfThePackage + iRandomDayOfThThirdPackage)
        {
            if (GetSpecialEvent((int)SpecialEvents.Paquete_Segundo) == true)
            {
                goWeirdPackage[(int)SpecialEvents.Paquete_Tercero].SetActive(true);
//                Debug.Log("Tercer paquete!!!!!!!");
                //El segundo paquete fue aceptado y llega el tercer paquete con el rifle
            }
            else
            {
                goWeirdPackage[(int)SpecialEvents.Paquete_Segundo].GetComponent<weirdPackageClicked>().DesactivateLabel();
                goWeirdPackage[(int)SpecialEvents.Paquete_Segundo].SetActive(false);
            }
        }
        if (iDays == iDayOfThePackage + iRandomDayOfTheFourthPackage)
        {
            if (GetSpecialEvent((int)SpecialEvents.Paquete_Tercero) == true)
            {
                goWeirdPackage[(int)SpecialEvents.Evento_Asesinato].SetActive(true);
                arraySpecialEvents[(int)SpecialEvents.Evento_Asesinato] = true;
//                myNewsController.setNextNodeTerroristAtack();
            }
            else
            {
                goWeirdPackage[(int)SpecialEvents.Paquete_Tercero].GetComponent<weirdPackageClicked>().DesactivateLabel();
                goWeirdPackage[(int)SpecialEvents.Paquete_Tercero].SetActive(false);
            }
        }
    }

    public void StartNewDay()
    {
        fDayDuration = 0f;
        myDayNightScript.ResetGradosGiro();

        bBlockDaysCounter = true;

        iDays++;
        showText(Localization.Get("Day ") + iDays);

        if (iDays < iTotalDays)
        {
            myNewsController.StopAllComicSounds();
            if (arraySpecialEvents[(int)SpecialEvents.Evento_Asesinato] == true)
            {
                myNewsController.setEndGameTerroristStrike();
            }
            else
            {
                myNewspaperScript.NewDayStarts();

                if (iDays > 0)
                    myNewsController.RemoveLastDay();

                Debug.Log("Activando las noticias de hoy!!!!! " + iDays);
                myNewsController.getNewsFromDay();

                //            myNewsController.getNewsFromDay(iDays); 
                if (myNewsController.getIsObjectToday(iDays) == true)
                {
                    myNewsController.addObjectsActivatedCounter();
                }

                CheckSpecialEvent(); //Se comprueba si ese día hay paquete
            }
        }
        else
        {
            //            myNewspaperScript.NewDayStarts();
            //            Debug.Log("He sobrepasado el número total de días y estoy repitiendo la última noticia");
            //            myNewsController.GetLastDayDEMO();
            if (myCharacterManager.getPoliticalAffinity() >= 8)
            {
                dystopicGameManager.myGameManagerInstance.EndGame((int)dystopicGameManager.EnumTypeOfEnd.WITH_HIGH_AFFINITY);
            } else if (myCharacterManager.getPoliticalAffinity() < 3)
            {
                dystopicGameManager.myGameManagerInstance.EndGame((int)dystopicGameManager.EnumTypeOfEnd.WITHOUT_AFFINITY);
            } else
                dystopicGameManager.myGameManagerInstance.EndGame((int)dystopicGameManager.EnumTypeOfEnd.WITH_MED_AFFINITY);
        }
    }

    public void ResetTextUpDown(Text sTextUP, Text sTextDOWN)
    {
        sTextUP.text = "";
        sTextDOWN.text = "";
        sTextDOWN.enabled = true;
    }

    //Comparador de parámetros cuando la TV está APAGADA
    private void Compare2Parameters(float fAnt, float fCurrent, float fExternalMod, Text sTextUP, Text sTextDOWN)
    {
        if (fAnt == fCurrent)
        {
            ResetTextUpDown(sTextUP, sTextDOWN);
        }
        else if (fAnt < fCurrent)
        {
            sTextUP.text = "+";
            if (fExternalMod > 4 && fExternalMod <= 8)
            {
                sTextUP.text = "++";
            }
            else if (fExternalMod > 8)
            {
                sTextUP.text = "+++";
            }

            sTextUP.enabled = true;
        }
        else
        {
            sTextDOWN.text = "-";
            if ((fExternalMod <= -2) && (fExternalMod <= -4))
            {
                sTextDOWN.text = "--";
            }
            else if (fExternalMod < -4)
                sTextDOWN.text = "---";

            sTextUP.enabled = false;
        }

        sTextDOWN.enabled = !sTextUP.enabled;
    }
    
    //Comparador de parámetros cuando la TV está ENCENDIDA
    private void Compare2ParametersTV(float fAnt, float fCurrent, float fExternalMod, Text sTextUP, Text sTextDOWN)
    {
//        float fTemp = fCurrent - fAnt;

        if (fAnt == fCurrent)
        {
            ResetTextUpDown(sTextUP, sTextDOWN);
        }
        else if (fAnt < fCurrent) //Subiendo
        {
            sTextUP.text = "+";
//            if (fExternalMod > 0.1f && fExternalMod <= 0.4f)
            if (fExternalMod > 2f && fExternalMod <= 8f)
            {
                sTextUP.text = "++";
            }
//            else if (fExternalMod > 0.4f)
            else if (fExternalMod > 8f)
            {
                sTextUP.text = "+++";
            }

            sTextUP.enabled = true;
        }
        else //Bajando
        {
            sTextDOWN.text = "-";
            if ((fExternalMod < -0.09f) && (fExternalMod >= -0.2f))
            {
                sTextDOWN.text = "--";
            }
            else if (fExternalMod < -0.2)
                sTextDOWN.text = "---";

            sTextUP.enabled = false;
        }

        sTextDOWN.enabled = !sTextUP.enabled;
    }

    private void CompareMoney(float fAnt, float fCurrent, float fExternalMod, Text sTextUP, Text sTextDOWN)
    {
        if (fAnt == fCurrent)
        {
            ResetTextUpDown(sTextUP, sTextDOWN);
        }
        else if (fAnt < fCurrent)
        {
            sTextUP.text = "+";
            if (fExternalMod >= 0.7f && fExternalMod <= 0.1f)
            {
                sTextUP.text = "++";
            }
            else if (fExternalMod > 0.1f)
            {
                sTextUP.text = "+++";
            }

            sTextUP.enabled = true;
        }
        else
        {
            sTextDOWN.text = "-";
            if ((fAnt - fCurrent > 2) && (fAnt - fCurrent <= 4))
            {
                sTextDOWN.text = "--";
            }
            else if (fAnt - fCurrent > 4)
                sTextDOWN.text = "---";

            sTextUP.enabled = false;
        }

        sTextDOWN.enabled = !sTextUP.enabled;
    }

    IEnumerator showHideText()
    {
        textMessages.enabled = true;
        setTextMessages("Day " + iDays);
        yield return new WaitForSeconds(3);
        setTextMessages("");
        textMessages.enabled = false;
        bBlockDaysCounter = false;
    }

    IEnumerator showHideText(string newString)
    {
        setTextMessages(newString);
        textMessages.enabled = true;
        yield return new WaitForSeconds(5);
        setTextMessages("");
        textMessages.enabled = false;
        bBlockDaysCounter = false;
    }

    private void Awake()
    {
        iDayOfThePackage = 1;
        iRandomDayOfTheFirstPackage = Random.Range(2, 5); //Día semilla en el que aparece el paquete
        iRandomDayOfTheSecondPackage = Random.Range(5, 7); //Día segundo paquete;
        iRandomDayOfThThirdPackage = Random.Range(7, 9); //Día segundo paquete;
        iRandomDayOfTheFourthPackage = Random.Range(9, 11); //Día segundo paquete;

        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        for (int i = 0; i <  arraySpecialEvents.Length; i++)
        {
            arraySpecialEvents[i] = false;
        }
    }

    private void Start()
    {
        //        iTotalDays = myNewsController.getNewsSize();
        //        iTotalDays = 2;

        for(int i = 0; i < goWeirdPackage.Length; i++)
        {
            goWeirdPackage[i].SetActive(false);
        }

        sHealth = Localization.Get("Health");
        sHunger = Localization.Get("Hunger");
        sComfort = Localization.Get("Comfort");
        sFun = Localization.Get("Fun");
        sMoney = Localization.Get("Money");
//        sDown = Localization.Get("Down");
//        sUp = Localization.Get("Up");
        sDown = "";
        sUp = "";

        textMessages.text = "";
/*
        textUPHealth.color = Color.green;
        textUPHunger.color = Color.green;
        textUPComfort.color = Color.green;
        textUPFun.color = Color.green;
        textUPMoney.color = Color.green;
*/
        textUPHealth.text = sUp;
        textUPHunger.text = sUp;
        textUPComfort.text = sUp;
        textUPFun.text = sUp;
        textUPMoney.text = sUp;

        textUPHealth.enabled = false;
        textUPHunger.enabled = false;
        textUPComfort.enabled = false;
        textUPFun.enabled = false;
        textUPMoney.enabled = false;
/*
        textDOWNHealth.color = Color.red;
        textDOWNHunger.color = Color.red;
        textDOWNComfort.color = Color.red;
        textDOWNFun.color = Color.red;
        textDOWNMoney.color = Color.red;
*/
        textDOWNHealth.text = sDown;
        textDOWNHunger.text = sDown;
        textDOWNComfort.text = sDown;
        textDOWNFun.text = sDown;
        textDOWNMoney.text = sDown;

        textDOWNHealth.enabled = false;
        textDOWNHunger.enabled = false;
        textDOWNComfort.enabled = false;
        textDOWNFun.enabled = false;
        textDOWNMoney.enabled = false;

        fHealthANT = myCharacterManager.getHealth();
        fHungertANT = myCharacterManager.getHunger();
        fComfortANT = myCharacterManager.getComfort();
        fFunANT = myCharacterManager.getFun();
        fMoneyANT = myCharacterManager.getMoney();

        //Día 0
        myNewsController.setCurrentNode(0);
        myNewsController.GetAllDataFromDay(0);
//        StartNewDay();
    }

    // Update is called once per frame
    void Update()
    {
        if(dystopicGameManager.myGameManagerInstance.GetGamePaused() == false)
        {
            if(iDays > 0)
            {
                myCharacterManager.MainFunction();
            }


            fHealthCURRENT = myCharacterManager.getHealth();
            fHungertCURRENT = myCharacterManager.getHunger();
            fComfortCURRENT = myCharacterManager.getComfort();
            fFunCURRENT = myCharacterManager.getFun();
            fMoneyCURRENT = myCharacterManager.getMoney();
            fPoliticalAffinityCURRENT = myCharacterManager.getPoliticalAffinity();

            imageHealth.fillAmount = fHealthCURRENT/100;
            imageHungry.fillAmount = fHungertCURRENT / 100;
            imageComfort.fillAmount = fComfortCURRENT / 100;
            imageFun.fillAmount = fFunCURRENT / 100;
            imagePoliticalAffinity.fillAmount = fPoliticalAffinityCURRENT/10;

    //        textHealth.text = sHealth + fHealthCURRENT.ToString("0");
    //        if (myCharacterManager.getHealth() <= 25)
            if (fHealthCURRENT <= 25)
            {
    //            textHealth.color = Color.red;
                imageHealth.color = Color.red;
            }
            else
            {
                //          textHealth.color = Color.white;
                imageHealth.color = colorCustom;
            }

    //        textHunger.text = sHunger + fHealthCURRENT.ToString("0");
    //        if (myCharacterManager.getHunger() <= 25)
            if (fHungertCURRENT <= 25)
            {
    //            textHunger.color = Color.red;
                imageHungry.color = Color.red;
            }
            else
            {
    //            textHunger.color = Color.white;
                imageHungry.color = colorCustom;
            }

    //        textComfort.text = sComfort + fComfortCURRENT.ToString("0");
    //        if (myCharacterManager.getComfort() <= 25)
            if (fComfortCURRENT <= 25)
            {
    //            textComfort.color = Color.red;
                imageComfort.color = Color.red;
            }
            else
            {
    //            textComfort.color = Color.white;
                imageComfort.color = colorCustom;
            }

    //        textFun.text = sFun + fFunCURRENT.ToString("0");
    //        if (myCharacterManager.getFun() <= 25)
            if (fFunCURRENT <= 25)
            {
    //            textFun.color = Color.red;
                imageFun.color = Color.red;
            }
            else
            {
    //            textFun.color = Color.white;
                imageFun.color = colorCustom;
            }
    /*
            if (fPoliticalAffinityCURRENT <= 3)
            {
                //            textFun.color = Color.red;
                imagePoliticalAffinity.color = Color.red;
            }
            else if(fPoliticalAffinityCURRENT <=7)
            {
                //            textFun.color = Color.white;
                imagePoliticalAffinity.color = Color.yellow;
            }
            else
                imagePoliticalAffinity.color = Color.green;
    */

            //textMoney.text = sMoney + myCharacterManager.getMoney().ToString("0.00");
            textMoney.text = ("$") + myCharacterManager.getMoney().ToString("0.00");

            if (myCharacterManager.getTVActive() == true)
            {
    //            Debug.Log("LA TELE ESTÄ ACTIVA!!!!");
                Compare2ParametersTV(fHealthANT, fHealthCURRENT, myCharacterManager.getHealthExternalMod(), textUPHealth, textDOWNHealth);
                Compare2ParametersTV(fHungertANT, fHungertCURRENT, myCharacterManager.getHungerExternalMod(), textUPHunger, textDOWNHunger);
                Compare2ParametersTV(fComfortANT, fComfortCURRENT, myCharacterManager.getComfortExternalMod(), textUPComfort, textDOWNComfort);
                Compare2ParametersTV(fFunANT, fFunCURRENT, myCharacterManager.getFunExternalMod(), textUPFun, textDOWNFun);
            }
            else
            {
                Compare2Parameters(fHealthANT, fHealthCURRENT, myCharacterManager.getHealthExternalMod(), textUPHealth, textDOWNHealth);

                //        textUPHunger.enabled = (fHungertANT < fHungertCURRENT);
                //        textDOWNHunger.enabled = !textUPHunger.enabled;

                Compare2Parameters(fHungertANT, fHungertCURRENT, myCharacterManager.getHungerExternalMod(), textUPHunger, textDOWNHunger);

                //        textUPComfort.enabled = (fComfortANT < fComfortCURRENT);
                //        textDOWNComfort.enabled = !textUPComfort.enabled;

                Compare2Parameters(fComfortANT, fComfortCURRENT, myCharacterManager.getComfortExternalMod(), textUPComfort, textDOWNComfort);

                //        textUPFun.enabled = (fFunANT < fFunCURRENT);
                //        textDOWNFun.enabled = !textUPFun.enabled;

                Compare2Parameters(fFunANT, fFunCURRENT, myCharacterManager.getFunExternalMod(), textUPFun, textDOWNFun);

                //        textUPMoney.enabled = (fMoneyANT < fMoneyCURRENT);
                //        textDOWNMoney.enabled = !textUPMoney.enabled;
            }
            CompareMoney(fMoneyANT, fMoneyCURRENT, myCharacterManager.getMoneyModifier(), textUPMoney, textDOWNMoney);


            fHealthANT = fHealthCURRENT;
            fHungertANT = fHungertCURRENT;
            fComfortANT = fComfortCURRENT;
            fFunANT = fFunCURRENT;
            fMoneyANT = fMoneyCURRENT;

            //        Debug.Log("valor para la comparación: " + (fMoneyANT < myCharacterManager.getMoney()));

    //        textHealth.text = "Health: " + myCharacterManager.getHealth().ToString("0");
    //        textHunger.text = "Hunger: " + myCharacterManager.getHunger().ToString("0");
    //        textComfort.text = "Comfort: " + myCharacterManager.getComfort().ToString("0");
    //        textFun.text = "Fun: " + myCharacterManager.getFun().ToString("0");
    //        textMoney.text = "Money: " + myCharacterManager.getMoney().ToString("0.00");



            fDayDuration += Time.deltaTime;
            if (fDayDuration >= 3.6f * fDaySpeed)
            {
                StartNewDay();
            }

    //        Debug.Log("Duración del día: " + fDayDuration);
    /*
            if (iDays == iTotalDays -1)
            {
                goWeirdPackage.SetActive(true);
            }
    */
        }
            
        }
}
