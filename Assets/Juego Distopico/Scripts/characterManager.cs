﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterManager : MonoBehaviour {
    public dystopicCharacterController myDystopicCharacterController;
    public useTV_v2 myTV;
//    public dystopicGameManager myDystopicGameManager;

    public float fMoney = 0;

    public float fHealthModifier = 0;
    public float fHungerModifier= 0;
    public float fComforthModifier = 0;
    public float fFunModifier = 0;
    public float fMoneyModifier = 0;

    public int iNumberOfDevices;
//    public useDevice[] arrayDevices;
//    private bool[] arrayActiveDevices;

    private bool bActiveTV = true;

    private float fHealth = 100;
    private float fHunger = 100;
    private float fComfort = 100;
    private float fFun = 100;
    private float fPoliticalAffinity = 5f;

    private float fExternalHealthModifier = 0;
    private float fExternalHungerModifier = 0;
    private float fExternalComfortModifier = 0;
    private float fExternalFunModifier = 0;
    private float fExternalMoneyModifier = 0;

    private bool bEndGameHealth = false;
    private bool bEndGameHunger = false;
    private bool bEndGameComfort = false;
    private bool bEndGameFun = false;
    private bool bGameEnded = false; //Ésta variable la uso para controlar que sólo se ejecuta una vez el EndGame();

    private bool bUseTVAllowed = true;

    private float fTotalMoneyEarned = 0; //En esta variable sumamos el dinero total ganado durante el juego.

    private bool bFirstDay = true; //Es el primer día?

    #region getters
    public float getHealth()
    {
        return fHealth;
    }

    public float getHunger()
    {
        return fHunger;
    }

    public float getComfort()
    {
        return fComfort;
    }

    public float getFun()
    {
        return fFun;
    }

    public float getMoney()
    {
        return fMoney;
    }

    public float getTotalMoney()
    {
        return fTotalMoneyEarned;
    }

    public bool getIsTVAllowed()
    {
        return bUseTVAllowed;
    }

    public bool getEnoughMoney(float fExpense)
    {
        return ((fMoney - fExpense) >= 0);
    }

    public float getPoliticalAffinity()
    {
        return fPoliticalAffinity;
    }

    public float getHealthExternalMod()
    {
        return fExternalHealthModifier;
    }

    public float getHungerExternalMod()
    {
        return fExternalHungerModifier;
    }

    public float getComfortExternalMod()
    {
        return fExternalComfortModifier;
    }

    public float getFunExternalMod()
    {
        return fExternalFunModifier;
    }

    public float getMoneyModifier()
    {
        return fMoneyModifier;
    }

    public bool getTVActive()
    {
        return bActiveTV = myTV.getTVOn();
    }
    #endregion

    #region setters
    public void setFirstDay(bool bValue)
    {
        bFirstDay = bValue;
    }

    public void setTVAllowed(bool bNewValue)
    {
        bUseTVAllowed = bNewValue;
    }

    public void setMoreHealth(float fNewValue)
    {
        fHealth += fNewValue;
    }

    public void setMoreHunger(float fNewValue)
    {
        fHunger += fNewValue;
    }
    public void setMoreComfort(float fNewValue)
    {
        fComfort += fNewValue;
    }
    public void setMoreFun(float fNewValue)
    {
        fFun += fNewValue;
    }

    public void setMoreMoney(float fNewValue)
    {
        fMoney += fNewValue;
        fTotalMoneyEarned += fNewValue;
    }

    public void setModifyMoney(float fNewValue)
    {
        fMoneyModifier = fNewValue;
    }

    /*
        public void setExternalHealthfModifier(float fNewValue, int iSeconds)
        {
            fExternalHealthModifier = fNewValue;
            StartCoroutine(resetHealth(iSeconds));
        }

        public void setExternalComfortfModifier(float fNewValue, int iSeconds)
        {
            fExternalComfortModifier = fNewValue;
            StartCoroutine(resetComfort(iSeconds));
        }

        public void setExternalHungerfModifier(float fNewValue, int iSeconds)
        {
            fExternalHungerModifier = fNewValue;
            StartCoroutine(resetHunger(iSeconds));
        }

        public void setExternalFunModifier(float fNewValue, int iSeconds)
        {
            fExternalFunModifier = fNewValue;
            StartCoroutine(resetFun(iSeconds));
        }
    */

/// <summary>
/// //////////////////
/// </summary>
/// <param name="fNewValue"></param>
    public void setExternalHealthfModifier(float fNewValue)
    {
        fExternalHealthModifier = fNewValue;
//        StartCoroutine(resetHealth(iSeconds));
    }

    public void setExternalComfortfModifier(float fNewValue)
    {
        fExternalComfortModifier = fNewValue;
//        StartCoroutine(resetComfort(iSeconds));
    }

    public void setExternalHungerfModifier(float fNewValue)
    {
        fExternalHungerModifier = fNewValue;
//        StartCoroutine(resetHunger(iSeconds));
    }

    public void setExternalFunModifier(float fNewValue)
    {
        fExternalFunModifier = fNewValue;
//        StartCoroutine(resetFun(iSeconds));
    }

    /// <summary>
    /// ////////////////////////
    /// </summary>
    /// <param name="fNewValue"></param>
    public void setExternalFunModifierWithoutSeconds(float fNewValue)
    {
        fExternalFunModifier = fNewValue;
    }

    public void setExternalMoneyModifier(float fNewValue)
    {
        fExternalMoneyModifier = fNewValue;
    }

    public void setTVActive()
    {
        myDystopicCharacterController.setNewState(dystopicCharacterController.characterState.WATCHING_TV);
//        bActiveTV = true;
        //        arrayDevices[iIndex].setCurrentDestination(bNewValue);
    }

    #endregion

    public void receiveModifiers(float[] afValues, int iSeconds)
    {
        setTVAllowed(false);
        /*
                setExternalHealthfModifier(afValues[0], iSeconds);
                setExternalHungerfModifier(afValues[1], iSeconds);
                setExternalComfortfModifier(afValues[2], iSeconds);
                setExternalFunModifier(afValues[3], iSeconds);
        */
        setExternalHealthfModifier(afValues[0]);
        setExternalHungerfModifier(afValues[1]);
        setExternalComfortfModifier(afValues[2]);
        setExternalFunModifier(afValues[3]);

        StartCoroutine(resetStats(3));
    }

    public void receiveModifiers(float[] afValues, int[] aiSeconds)
    {
        setTVAllowed(false);
/*
        setExternalHealthfModifier(afValues[0], aiSeconds[0]);
        setExternalHungerfModifier(afValues[1], aiSeconds[1]);
        setExternalComfortfModifier(afValues[2], aiSeconds[2]);
        setExternalFunModifier(afValues[3], aiSeconds[3]);
*/
        setExternalHealthfModifier(afValues[0]);
        setExternalHungerfModifier(afValues[1]);
        setExternalComfortfModifier(afValues[2]);
        setExternalFunModifier(afValues[3]);

        StartCoroutine(resetStats(4));
    }

    public void restaMoney(float fCost)
    {
        if(bFirstDay == false)
            fMoney -= fCost;
    }

    public void sumaPoliticalAffinity(float fValue)
    {
        fPoliticalAffinity += fValue;
        if (fPoliticalAffinity > 10)
            fPoliticalAffinity = 10;
        if (fPoliticalAffinity < 0)
            fPoliticalAffinity = 0;
    }

    IEnumerator resetStats(int iSeconds)
    {
        yield return new WaitForSeconds(iSeconds);
        setTVAllowed(true);

        fExternalHealthModifier = 0;
        fExternalHungerModifier = 0;
        fExternalComfortModifier = 0;
        fExternalFunModifier = 0;
    }

    IEnumerator resetHealth(int iSeconds)
    {
        yield return new WaitForSeconds(iSeconds);
        setTVAllowed(true);

        fExternalHealthModifier = 0;
    }


    IEnumerator resetHunger(int iSeconds)
    {
        yield return new WaitForSeconds(iSeconds);
        setTVAllowed(true);

        fExternalHungerModifier = 0;
    }

    IEnumerator resetComfort(int iSeconds)
    {
        yield return new WaitForSeconds(iSeconds);
        setTVAllowed(true);

        fExternalComfortModifier = 0;
    }

    IEnumerator resetFun(int iSeconds)
    {
        yield return new WaitForSeconds(iSeconds);
        setTVAllowed(true);

        fExternalFunModifier = 0;
    }

    public void MainFunction()
    {
        if (bFirstDay == true)
            bFirstDay = false;

        fHealth += (fHealthModifier + fExternalHealthModifier) * Time.deltaTime;
        if (fHealth > 100) fHealth = 100;
        if (fHealth < 0)
        {
            fHealth = 0;
            bEndGameHealth = true;
        }

        fHunger += (fHungerModifier + fExternalHungerModifier) * Time.deltaTime;
        if (fHunger > 100) fHunger = 100;
        if (fHunger < 0)
        {
            fHunger = 0;
            bEndGameHunger = true;
        }

        fComfort += (fComforthModifier + fExternalComfortModifier) * Time.deltaTime;
        if (fComfort > 100) fComfort = 100;
        if (fComfort < 0)
        {
            fComfort = 0;
            bEndGameComfort = true;
        }

        fFun += (fFunModifier + fExternalFunModifier) * Time.deltaTime;
        if (fFun > 100) fFun = 100;
        if (fFun < 0)
        {
            fFun = 0;
            bEndGameFun = true;
        }

        fTotalMoneyEarned += fMoneyModifier * Time.deltaTime;
        //        Debug.Log("Dinero ganado: " + fTotalMoneyEarned);

        fMoney += (fMoneyModifier + fExternalMoneyModifier) * Time.deltaTime;

        //Condición de fin: el jugador está muerto.
        if (bEndGameHealth || bEndGameHunger || bEndGameComfort || bEndGameFun)
            if (bGameEnded == false)
            {
                dystopicGameManager.myGameManagerInstance.EndGame((int)dystopicGameManager.EnumTypeOfEnd.PLAYER_DIED);//Game over
                bGameEnded = true;
            }

        //        Debug.Log("Political Affinity: " + fPoliticalAffinity);

        if (myDystopicCharacterController.getWatchingTV() == true)
        {
            //            Debug.Log("Viendo la tele...");
            setModifyMoney(fMoneyModifier);
        }

    }
    // Use this for initialization
    void Start() {

//        arrayActiveDevices[0] = true;
        bActiveTV = myDystopicCharacterController.getWatchingTV();
	}
}
