﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallsController : MonoBehaviour {
    public GameObject[] aGOWalls;
    public GameObject goWindow;
    public GameObject goBathroomDoor;
    public GameObject goMainDoor;

    public Material[] aWallMaterials;
	// Use this for initialization
	void Start () {
        int iRandomTemp = Random.Range(0,aWallMaterials.Length);
		for(int i = 0; i < aGOWalls.Length; i++)
        {
            aGOWalls[i].GetComponent<Renderer>().material = aWallMaterials[iRandomTemp];
        }
        goWindow.GetComponent<Renderer>().material = aWallMaterials[iRandomTemp];
        goBathroomDoor.GetComponent<Renderer>().material = aWallMaterials[iRandomTemp];
        goMainDoor.GetComponent<Renderer>().material = aWallMaterials[iRandomTemp];

    }
}
