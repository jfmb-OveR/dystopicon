﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bedDestroyed : MonoBehaviour {

    public GameObject goDestroyedBed;
    public GameObject goNormalBed;

    public GameObject goExplosion;

    void OnTriggerEnter(Collider newCollider)
    {
        if(newCollider.tag == "Molotov")
        {
            goExplosion.SetActive(true);
            goDestroyedBed.SetActive(true);
            goNormalBed.SetActive(false);
            newCollider.gameObject.SetActive(false);
            //Destroy(newCollider.gameObject);
        }
    }

    void Start()
    {
        goExplosion.SetActive(false);
        goDestroyedBed.SetActive(false);
    }
}
