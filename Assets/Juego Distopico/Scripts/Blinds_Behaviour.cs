﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinds_Behaviour : MonoBehaviour {
    private Animator animDevice;
    private int isOpenHash;

    public void OpenBlinds(bool bValue)
    {
        animDevice.SetBool(isOpenHash, bValue);
    }
 
    void Start ()
    {
        animDevice = this.GetComponent<Animator>();
        isOpenHash = Animator.StringToHash("openDevice");
    }
}
