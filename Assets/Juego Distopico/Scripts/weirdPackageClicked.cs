﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weirdPackageClicked : MonoBehaviour {
    public weirdPackageScript myWeirdPackage;
    public int iPackageIndex;
    public GameObject goLabelPackage;
    public UILabel labelTakeIt;

    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;

    void OnMouseDown()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        labelTakeIt.enabled = false;
        myWeirdPackage.ActivateMessage(iPackageIndex);
        DesactivateLabel();
    }

    private void OnMouseOver()
    {
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);

        labelTakeIt.enabled = true;
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        labelTakeIt.enabled = false;
    }

    public void DesactivateLabel()
    {
        goLabelPackage.SetActive(false);
    }

    private void Awake()
    {
        labelTakeIt.enabled = false;
        goLabelPackage.SetActive(true);
    }

    private void OnDestroy()
    {
        if(labelTakeIt != null)
            labelTakeIt.enabled = false;
    }
}
