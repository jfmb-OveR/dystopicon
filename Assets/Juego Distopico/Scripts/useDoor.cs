﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class useDoor : MonoBehaviour {
    public int iIndex;
    public myAnalyticsScriptForDevices myAnalytics;

    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;

    public dystopicSceneManager mySceneManager;
    public characterManager myCharacterManager;
    public Transform tPlayerDestination;
    public float fMoneyModifier;

    public Text textDevice;
    public UILabel labelDevice;

    public AudioSource soundUse;
    public AudioSource soundCoin;
    public Renderer materialLuzCandado;
    public Material materialVerdeCandado;
    public Material materialRojoCandado;
    public AudioSource soundError;
    public AudioSource audioOnMouseOver;
    private Material materialMainCandado;

    private bool bIsActive = false;
    private bool bOnMouseOverFirstTime = true;

    public float getCost()
    {
        return fMoneyModifier;
    }

    private void OnMouseOver()
    {
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);

        //        textDevice.enabled = true;
        if (bOnMouseOverFirstTime == true)
        {
            audioOnMouseOver.Play();
            bOnMouseOverFirstTime = false;
        }

        labelDevice.enabled = true;
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        //        textDevice.enabled = false;
        bOnMouseOverFirstTime = true;
        labelDevice.enabled = false;
    }

    void OnMouseDown()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        myAnalytics.increaseTimeUsed();
        
        if (myCharacterManager.getEnoughMoney(fMoneyModifier) == true)
        {
            mySceneManager.setTextMessages(Localization.Get("DoorSuccess"));
            //            mySceneManager.setTextMessages("Para abrir la puerta un permiso del gobierno.");
            materialLuzCandado.material = materialVerdeCandado;
            soundCoin.Play();
            soundUse.Play();

            StartCoroutine(CorEndGame());
        }
        else
        {
            //            mySceneManager.setTextMessages("Para salir necesita un permiso gubernamental."); ;
            soundError.Play();
            materialLuzCandado.material = materialRojoCandado;

            mySceneManager.setTextMessages(Localization.Get("DoorNotSuccess"));
            StartCoroutine(showHideText());
        }

    }

    private void Start()
    {
//        textDevice.enabled = false;
        labelDevice.enabled = false;
    }

    IEnumerator showHideText()
    {
        yield return new WaitForSeconds(3);
        mySceneManager.setTextMessages(""); ;
    }

    IEnumerator CorEndGame()
    {
        yield return new WaitForSeconds(3);
        dystopicGameManager.myGameManagerInstance.EndGame((int)dystopicGameManager.EnumTypeOfEnd.PAY_500);
    }
}
