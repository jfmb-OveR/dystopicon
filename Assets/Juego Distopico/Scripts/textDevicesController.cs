﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textDevicesController : MonoBehaviour {

    public void ShowHideText(Text myText)
    {
        StartCoroutine(showHideText(myText));
    }

    IEnumerator showHideText(Text myText)
    {
        //TODO: texto para traducir
        myText.enabled = true;
         yield return new WaitForSeconds(3);
        myText.enabled = false;
    }
}
