﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {
    [SerializeField]
    private GameObject goImagenFondoLoading;
    [SerializeField]
    private Text textImagenFondoLoading;
    [SerializeField]
    private Text textLoading;
    //private UILabel labelLoading;
    [SerializeField]
    private GameObject goLanguageSelector;
    [SerializeField]
    private GameObject goCredits;
    [SerializeField]
    private UILabel labelDystopicon;
    public GameObject goTextLoading;
    public GameObject goTextReady;

    private bool bLoadScene = false;
    private bool bSceneActivationAllowed = false;

    public void AllowSceneActivation(){
        bSceneActivationAllowed = true;
    }

    public void ShowCredits()
    {
        goCredits.SetActive(!goCredits.activeSelf);
        labelDystopicon.enabled = !goCredits.activeSelf;
        goLanguageSelector.SetActive(false);
    }

    public void HideCredits()
    {
        labelDystopicon.enabled = enabled;
        goCredits.SetActive(false);
    }

    public void ShowHideLanguageSelector()
    {
        HideCredits();
        //        labelDystopicon.enabled = !labelDystopicon.enabled;
        //        goLanguageSelector.SetActive(!goLanguageSelector.activeSelf);
        goLanguageSelector.SetActive(!goLanguageSelector.activeSelf);
        labelDystopicon.enabled = !goLanguageSelector.activeSelf;
    }



    public void LoadGame()
    {
        goImagenFondoLoading.SetActive(true);
        textImagenFondoLoading.text = Localization.Get("SaludoInicial");
        textImagenFondoLoading.enabled = true;
        bLoadScene = true;
        //        StartCoroutine(LoadingMessage02());
        goTextLoading.SetActive(true);
        StartCoroutine(WaitBeforeLoading(15));
    }

    public bool CheckSceneActivationAllowed(){
        return bSceneActivationAllowed;        
    }

    public void Quit()
    {
        Application.Quit();
    }
/*
    IEnumerator LoadingMessage02()
    {
        yield return new WaitForSeconds(3);
        textLoading.text = Localization.Get("Loading02");
        StartCoroutine(LoadingMessage03());
    }

    IEnumerator LoadingMessage03()
    {
        yield return new WaitForSeconds(3);
        textLoading.text = Localization.Get("Loading03");
    }
*/
    IEnumerator WaitBeforeLoading(int iSeconds)
    {
//        yield return new WaitForSeconds(iSeconds);
        yield return new WaitForSeconds(0);
        StartCoroutine(LoadScene(1));
    }

    IEnumerator LoadScene(int newScene)
    {
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);

        async.allowSceneActivation = false;

        while(!async.isDone){
            Debug.Log("Progress: " + async.progress);
            if(async.progress >= 0.9f)
            {
                Debug.Log("Estoy ready...");
                textLoading.enabled = false;
                goTextReady.SetActive(true);

                if(CheckSceneActivationAllowed() == true)
                    async.allowSceneActivation = true;
            }
            yield return null;
        }
    }

    void Start()
    {
        //        labelLoading = this.GetComponent<UILabel>();
        goImagenFondoLoading.SetActive(false);
        textImagenFondoLoading.enabled = false;

        goTextLoading.GetComponent<Text>().text = Localization.Get("Loading01");
        goTextLoading.SetActive(false);

        goTextReady.GetComponent<Text>().text = Localization.Get("ButtonReadyToStart");
        goTextReady.SetActive(false);

        labelDystopicon.enabled = true;

        goLanguageSelector.SetActive(false);
        goCredits.SetActive(false);
    }
/*
    void Update()
    {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
        }
    }
*/
}
