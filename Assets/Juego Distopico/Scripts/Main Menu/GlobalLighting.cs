﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalLighting : MonoBehaviour {
	public Color colorGlobal;

	[SerializeField]
	private float fTargetColor = 0.25f;
	private float fActualColor = 0f;


	private bool bChangeToWhite = false;
    void SetGlobalLightingWhite(){
        bChangeToWhite = true;
    }
    

	// Use this for initialization
	void Start () {
        RenderSettings.ambientLight = colorGlobal;
	}
	
	// Update is called once per frame
	void Update () {
		if(bChangeToWhite == true){
			if(fActualColor < fTargetColor){
				fActualColor += Time.deltaTime;
				RenderSettings.ambientLight = new Color(fActualColor, fActualColor, fActualColor);
			}
			else
				bChangeToWhite = false;
		}
	}
}
