﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyScript : MonoBehaviour {
    private UILabel labelMoney;
    private int iDollars = 1;
    public void IncreaseMoney()
    {
        iDollars++;
        labelMoney.text = iDollars + "$";
    }

    void Start(){
        labelMoney = this.GetComponent<UILabel>();
    }
}
