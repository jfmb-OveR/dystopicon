﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TvEmissionScript : MonoBehaviour {
    Renderer renderer;
    Material mat;

    float emission;
    float floor = 0.3f;
    float ceiling = 1.0f;

    Color baseColor = Color.white;
    
    Color finalColor;

    // Use this for initialization
    void Start () {
        renderer = GetComponent<Renderer>();
        mat = renderer.material;
    }
	
	// Update is called once per frame
	void Update () {
        //        emission = Mathf.PingPong(Time.time, 1.0f);
        emission = floor + Mathf.PingPong(Time.time, ceiling - floor);

        finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        mat.SetColor("_EmissionColor", finalColor);
    }
}
