﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatsController : MonoBehaviour {
    public GameObject myPrefab;
    public int iMaxRats = 5;
    private int iCurrentRats;

//    public void AllRatsActivated(bool bValue)

    public void AllRatsActivated(bool bValue)
    {
        for (int i = 0; i < this.gameObject.transform.childCount; i++)
        {
            if(this.gameObject.transform.GetChild(i).gameObject.activeSelf == true)
            {
                this.gameObject.transform.GetChild(i).gameObject.GetComponent<Rat_Behaviour>().WalkRat(bValue);
                Debug.Log("555555555555555555555555555 Rata: " + i);
            }
        }
    }

    public void IncreseRats()
    {
        iCurrentRats++;
    }

    public void DecreseRats()
    {
        iCurrentRats--;
    }

    private void NewRat()
    {
        GameObject goNewRat = new GameObject();
        goNewRat = Instantiate(myPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        goNewRat.transform.SetParent(this.gameObject.transform);
        goNewRat.SetActive(true);

        IncreseRats();
    }

    private void ActivateFirstRat()
    {
        StartCoroutine(CorActivateFirstRat());
    }

    private void SpawnRat()
    {
        if(iCurrentRats <= iMaxRats)
        {
            StartCoroutine(CorSpawnRat());
        }
    }

    IEnumerator CorActivateFirstRat()
    {
        yield return new WaitForSeconds(Random.Range(10, 20));
        NewRat();
        SpawnRat();
    }

    IEnumerator CorSpawnRat()
    {
        yield return new WaitForSeconds(Random.Range(20, 50));
        NewRat();
        SpawnRat();
    }

    void Start()
    {
        myPrefab.SetActive(false);
        ActivateFirstRat();

//        Instantiate(myPrefab, new Vector3(0, 0, 0), Quaternion.identity);
    }
}
