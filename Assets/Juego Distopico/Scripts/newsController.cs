﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class newsController : MonoBehaviour {
    public dystopicGameManager myGameManager;
    public int iNewsSize;
    public int iWindowNewsSize;
    public GameObject goBackgroundComicWindow;
    public int iObjectsToActivate;
    public int iFirstObjectIndexToActivate = 8;
    public GameObject goNews;
    public Image imageNews;
    public Text textNews;

    public Button buttonImageNews;

    public GameObject goFinalNews;
    public Image imageFinalNews;
    public Text textFinalNews;
    public Button buttonImageFinalNews;

    public GameObject goPoliceLights;
    [Header("Riots, Police Control, Inmigration, Political Speech, Service, Robots,Food")]
    public AudioSource[] soundComics;

    public string[] arrayNews;
    private bool[] arrayWindowsHasEvent; //tiene el mismo tamaño que el arrayNews
    private bool[] arrayThisDayActivatesObject; //tiene el mismo tamaño que el arrayNews. Indica si ese día se activa un objeto.
    private string[] arrayWindowEvents; //Es de tamaño más pequeño. Sólo pasan cosas en la ventana algunos días. Al día siguiente la noticia suele ser sobre lo que ha salido en la ventana.
    private int[] arrayIDObjects; //En este array se guardan los ID de los objetos que se activarán en días determinados. Se activan según el orden en el que están (POR AHORA!)

    public GameObject[] arrayGoSpritesWindow; //Imágenes de cómic de lo que ocurre en la ventana.
    [Header("Modificador de Afinidad política de cada cómic")]
    public float[] afPoliticalAffinity; //Array de afinidades políticas.

    private int iNewsFromWindowCounter = 0; //Lleva la cuenta de los eventos que han pasado en la ventana.
    private int iObjectsActivatedCounter = -1; //Índice de los objetos que se pueden activar

    private float fPoliticalAffinityTEMP = 0f; //Guarda la afinidad política del último cómic visto.

    int iSelectedNode = -1; //Lleva la cuenta del nodo seleccionado ese día.


    private enum enumAudioComics
    {
        RIOTS = 0,
        POLICE_CONTROL = 1,
        INMIGRANTS = 2,
        POLITICAL_SPEECH = 3,
        SERVICE = 4,
        ROBOTS = 5,
        FOOD = 6,
        CAR_BOMB = 7
    }

    private class NewsNodes
    {
        int iID = -1;
        int iPreviousNode = -1; //Si tiene un valor >= 0 es porque tiene un nodo que obligatoriamente va antes. Si es -1 no.
        int iNextNode = -1; //Si tiene un valor >= 0 es porque tiene un nodo que obligatoriamente va después. Si es -1 no.

        string sTodayNews = "";
        bool bWindowHasEvent = false;
        bool bThisDayActivatesObject = false;

        int iIDObjectToActivate = -1;
        int iIDWindowsEvent = -1; //ID del evento de la ventana (si lo tiene)


//        bool bHasNodeAfterThis = false;

        #region gettters
        public int getID()
        {
            return iID;
        }

        public int getIDPreviousNode()
        {
            return iPreviousNode;
        }

        public int getIDNextNode()
        {
            return iNextNode;
        }

        public string getTodayNews()
        {
            return sTodayNews;
        }

        public bool getWindowHasEvent()
        {
            return bWindowHasEvent;
        }

        public bool getThisDayActivatesObject()
        {
            return bThisDayActivatesObject;
        }
        public int getIDObjectToActivate()
        {
            return iIDObjectToActivate;
        }

        public int getIDWindowsEvent()
        {
            return iIDWindowsEvent;
        }

        #endregion

        #region setters
        public void setID(int iNewValue)
        {
            iID = iNewValue;
        }

        public void setPreviousNode(int iNewValue)
        {
            iPreviousNode = iNewValue;
        }

        public void setNextNode(int iNewValue)
        {
            iNextNode = iNewValue;
        }

        public void setTodayNews(string newString)
        {
            sTodayNews = newString;
        }

        public void setWindowHasEvent(bool bNewValue)
        {
            bWindowHasEvent = bNewValue;
        }

        public void setThisDayActivatesObject(bool bNewValue)
        {
            bThisDayActivatesObject = bNewValue;
        }

        public void setObjectToActivate(int iNewValue)
        {
            iIDObjectToActivate = iNewValue;
        }

        public void setIDWindowsEvent(int iNewValue)
        {
            iIDWindowsEvent = iNewValue;
        }
        /*
                public void setHasNodeAfterThis(bool bNewValue)
                {
                    bHasNodeAfterThis = bNewValue;
                }
        */
        #endregion
    }

    private List<NewsNodes> listOfNews = new List<NewsNodes>();
    private bool[] arrayNodesUsed;
    private bool bHasNextNode = false;
    private int iNextNodeToInsert = -1;
    private int iCurrentNode = -1;

    public int getIndexNodeWithID(int iIdToFind)
    {
        int iSize = listOfNews.Count;
        int iIndexToReturn = -1;
        for (int i = 0; i < listOfNews.Count; i++)
        {
            if (listOfNews[i].getID() == iIdToFind)
            {
                Debug.Log("He entrado en el id del next" + iIdToFind);
                iIndexToReturn = i;
                break;
            }
        }

        return iIndexToReturn;
    }

    public int getNewsSize()
    {
        return iNewsSize;
    }

    public int getCurrentNodey()
    {
        return iCurrentNode;
    }

    public float getPoliticalAffinityCurrentComic()
    {
        return fPoliticalAffinityTEMP;
    }

    public void setCurrentNode(int iNewValue)
    {
        iCurrentNode = iNewValue;
    }

    public void setPoliticalAffinityTEMP(float fValue)
    {
        fPoliticalAffinityTEMP = fValue;
    }

    /*
        public void setNextNodeTerroristAtack() //Se introduce el nodo del ataque terrorista
        {
            bHasNextNode = true;
            iNextNodeToInsert = 17; //Nodo del ataque terrorista
        }
    */
    private void startArrayNews()
    {
        arrayWindowsHasEvent[0] = false;
        //"¡Recuerda! El Gobierno de la Trinidad te proporciona una casa y lo necesario para satisfacer tus necesidades vitales a cambio de una módica cantidad. Podrás hacer frente a estos gastos viendo la televisión.  \n¡Cuánto más tiempo pases frente al aparato mejor será tu calidad de vida!\n¡Y si tu comportamiento es ejemplar, recibirás más dispositivos para hacer tu vida más confortable! \n¡Justicia y Trabajo!"
        arrayNews[0] = Localization.Get(arrayNews[0]);
        arrayThisDayActivatesObject[0] = false;

        arrayWindowsHasEvent[1] = false;
        //"Por su buen comportamiento y su compromiso con la labor del gobierno, se le ha concedido el privilegio de participar en la lotería nacional.\nPor un módico precio podrá participar en el sorteo de un trabajo fijo. \n¡Qué diablos! ¡Además es divertido! \n¡Justicia y Trabajo!"
        arrayNews[1] = Localization.Get(arrayNews[1]);
        arrayThisDayActivatesObject[1] = true;

        arrayWindowsHasEvent[2] = false;
        //"Las nuevas altas de subsidio, condecidas por el Gobierno de la Trinidad, han aumentado esta semana un 0.5%. Eso significa que cada día más personas pueden tener sus necesidades vitales cubiertas. \n¡Justicia y Trabajo!"
        arrayNews[2] = Localization.Get(arrayNews[2]);
        arrayThisDayActivatesObject[2] = false;

        arrayWindowsHasEvent[3] = false;
        //        "El gobierno sabe que los días empiezan a ser fríos. Por ello La Trinidad quiere regalarle una estilosa estufa que, sin duda, hará más confortable su vivienda. \n¡Justicia y Trabajo!"
        arrayNews[3] = Localization.Get(arrayNews[3]);
        arrayThisDayActivatesObject[3] = true;

        arrayWindowsHasEvent[4] = false;
        // "Los datos de audiencia de esta semana prometen superar a los de la semana anterior. Todas las cadenas experimentan cifras mejores que los resultados previos. \n¡Justicia y Trabajo!"
        arrayNews[4] = Localization.Get(arrayNews[4]);
        arrayThisDayActivatesObject[4] = false;

        arrayWindowsHasEvent[5] = true;
        // "Miles de personas se están manifestando demandando más libertad para salir a la calle. Le pedimos que permanezca en su casa y no se asomen a la ventana ya que podrían resultar heridos o algo peor. \nEl Gobierno de la Trinidad le recuerda que estas medidas se tomaron para garantizar la seguridad de todos los ciudadanos y fue aprobada por mayoría de la población en referendum. \n¡Justicia y Trabajo!""       
        arrayNews[5] = Localization.Get(arrayNews[5]);
        arrayThisDayActivatesObject[5] = false;

        arrayWindowsHasEvent[6] = false;
        //   El violento golpe de estado que ayer trató de derrocar al Gobierno de la Trinidad pudo ser contenido por las fuerzas de seguridad aunque a un alto precio. 23 polícias murieron y 5 opositores, 2 de ellos mientras manipulaban un cocktail molotov. \nPor haberse quedado en casa, no haber apoyado a los alborotadores y no haberse asomado a la ventana el Ministerio de Seguridad y Orden Público, junto al Ministerio de Comfort Ciudadano han decidido premiarle con una estufa que, sin duda, harán más agradables los días fríos. \n¡Justicia y Trabajo!"
        arrayNews[6] = Localization.Get(arrayNews[6]);
        arrayThisDayActivatesObject[6] = true;

        arrayWindowsHasEvent[7] = true;
        //"¡Últimas noticias! \nEl Secretario de Estado para la seguridad ha sido detenido por circular por la calle fuera de su horario autorizado. Ha sido destituido automáticamente y será juzgado por quebrantar la ley. ¡La justicia es igual para todos y los miembros del gobierno deben dar ejemplo! \n¡Justicia y trabajo!","¡Últimas noticias! \nEl Secretario de Estado para la seguridad ha sido detenido por circular por la calle fuera de su horario autorizado. Ha sido destituido automáticamente y será juzgado por quebrantar la ley. ¡La justicia es igual para todos y los miembros del gobierno deben dar ejemplo! \n¡Justicia y trabajo!"
        arrayNews[7] = Localization.Get(arrayNews[7]);
        arrayThisDayActivatesObject[7] = false;

        arrayWindowsHasEvent[8] = false;
        //Como bien sabe, las drogas ilegales son perjudiciales para la salud. Por eso el gobierno le proporciona unas drogas legales que no solo no son un riesgo para usted sino que además son divertidas. \nLos impuestos recaudados se destinarán a nuevos subsidios. \n¡Justicia y Trabajo!
        arrayNews[8] = Localization.Get(arrayNews[8]);
        arrayThisDayActivatesObject[8] = true;

        arrayWindowsHasEvent[9] = false;
        //"Desde el Ministerio de Comunicación se están estudiando los gustos de los espectadores para adaptar mejor los contenidos. Dispondrá de nuevos canales próximamente. El objetivo del gobierno es, ante todo, su bienestar. \n¡Justicia y Trabajo!"
        arrayNews[9] = Localization.Get(arrayNews[9]);
        arrayThisDayActivatesObject[9] = false;
  

        arrayWindowsHasEvent[10] = false;
        //        La Trinidad le recuerda que usted disfruta de esta vivienda gracias a que un inquilino anterior decidió acabar con su vida.El suicidio es legal y ético, permite a otra persona disfrutar de una casa.\nPara ello tiene a su disposición la máquina de eutanasia. Valore usarla si no encuentra suficiente motivación en el día a día.\n\n¡Justicia y trabajo!
        arrayNews[10] = Localization.Get(arrayNews[10]);
        arrayThisDayActivatesObject[10] = false;

        arrayWindowsHasEvent[11] = false;
        //"¡Felicidades! Hoy se activa el nuevo canal de deportes.\nPodrá disfrutar de los partidos más apasionantes de las mejores ligas y torneos nacionales.\nTodos los días, tras un breve discruso y después de escuchar nuestro himno nacional, ¡un nuevo partido!\n\n¡Justicia y Trabajo!"
        arrayNews[11] = Localization.Get(arrayNews[11]);
        arrayThisDayActivatesObject[11] = true;

        arrayWindowsHasEvent[12] = true;
        //Ventana: expulsion
        arrayNews[12] = Localization.Get(arrayNews[12]);
        arrayThisDayActivatesObject[12] = false;

        arrayWindowsHasEvent[13] = true;
        //Ventana: Acto
        arrayNews[13] = Localization.Get(arrayNews[13]);
        arrayThisDayActivatesObject[13] = false;

        arrayWindowsHasEvent[14] = true;
        //Ventana: Comida
        arrayNews[14] = Localization.Get(arrayNews[14]);
        arrayThisDayActivatesObject[14] = false;

        arrayWindowsHasEvent[15] = true;
        //Ventana: Mitín
        arrayNews[15] = Localization.Get(arrayNews[15]);
        arrayThisDayActivatesObject[15] = false;

        arrayWindowsHasEvent[16] = true;
        //Ventana: Robots
        arrayNews[16] = Localization.Get(arrayNews[16]);
        arrayThisDayActivatesObject[16] = false;

        arrayWindowsHasEvent[17] = false;
        //Promoción de ciudadanos.
        arrayNews[17] = Localization.Get(arrayNews[17]);
        arrayThisDayActivatesObject[17] = false;

        arrayWindowsHasEvent[18] = false;
        //RADIO
        arrayNews[18] = Localization.Get(arrayNews[18]);
        arrayThisDayActivatesObject[18] = true;

        arrayWindowsHasEvent[19] = false;
        //TV Forest
        arrayNews[19] = Localization.Get(arrayNews[19]);
        arrayThisDayActivatesObject[19] = true;

        arrayWindowsHasEvent[20] = false;
        //Canal Porno
        arrayNews[20] = Localization.Get(arrayNews[20]);
        arrayThisDayActivatesObject[20] = true;

        arrayWindowsHasEvent[21] = false;
        //Canal Noticias
        arrayNews[21] = Localization.Get(arrayNews[21]);
        arrayThisDayActivatesObject[21] = true;

        arrayWindowsHasEvent[22] = false;
        //Computadora
        arrayNews[22] = Localization.Get(arrayNews[22]);
        arrayThisDayActivatesObject[22] = true;

        arrayWindowsHasEvent[23] = false;
        //Canal teléfono
        arrayNews[23] = Localization.Get(arrayNews[23]);
        arrayThisDayActivatesObject[23] = true;

        arrayWindowsHasEvent[24] = false;
        //Canal Medkit
        arrayNews[24] = Localization.Get(arrayNews[24]);
        arrayThisDayActivatesObject[24] = true;

        arrayWindowsHasEvent[25] = false;
        //Promoción de ciudadanos.
        arrayNews[25] = Localization.Get(arrayNews[25]);
        arrayThisDayActivatesObject[25] = false;

        arrayWindowsHasEvent[26] = false;
        //Promoción de ciudadanos.
        arrayNews[26] = Localization.Get(arrayNews[26]);
        arrayThisDayActivatesObject[26] = false;

        arrayWindowsHasEvent[27] = false;
        //Promoción de ciudadanos.
        arrayNews[27] = Localization.Get(arrayNews[27]);
        arrayThisDayActivatesObject[27] = false;

        arrayWindowsHasEvent[28] = false;
        //Promoción de ciudadanos.
        arrayNews[28] = Localization.Get(arrayNews[28]);
        arrayThisDayActivatesObject[28] = false;

        arrayWindowsHasEvent[29] = false;
        //Promoción de ciudadanos.
        arrayNews[29] = Localization.Get(arrayNews[29]);
        arrayThisDayActivatesObject[29] = false;

        arrayWindowsHasEvent[30] = true;
        //Ventana: Cómic coche bomba
        arrayNews[30] = Localization.Get(arrayNews[30]);
        arrayThisDayActivatesObject[30] = false;


        startListOfNodes();
    }

    void startListOfNodes()
    {
        int iIDObjectToActivate = iFirstObjectIndexToActivate;

        // TODO sustituir el bucle por unas asignaciones estáticas como con el array anterior.
        for (int i = 0; i < arrayNews.Length; i++)
        {
            NewsNodes newNode = new NewsNodes();
            newNode.setID(i);

            switch (i)
            {
                case (5): //Ventana: comic Manifestación en la ventana
                    newNode.setNextNode(6);
                    newNode.setIDWindowsEvent(0);
                    Debug.Log("El nodo " + i + " tiene un nodo posterior: " + (6));
                    break;
                case (6): //El jugador miró por la ventana durante la manifestación
                    Debug.Log("El nodo " + i + " tiene un nodo anterior: " + (5));
                    newNode.setPreviousNode(5);
                    break;
                case (7): //Ventana: comic Control de policía
                    newNode.setIDWindowsEvent(1);
                    break;
                case (9): //Posibles nuevos canales
                    newNode.setNextNode(11);
                    break;
                case (11): //Nuevo canal de deportes
                    newNode.setPreviousNode(9);
                    break;
                case (12): //Ventana: comic expulsión
                    newNode.setIDWindowsEvent(2);
                    break;
                case (13): //Ventana:comic  acto
                    newNode.setIDWindowsEvent(3);
                    break;
                case (14): //Ventana: comic comida
                    newNode.setIDWindowsEvent(4);
                    break;
                case (15): //Ventana: comic Mitin
                    newNode.setIDWindowsEvent(5);
                    break;
                case (16): //Ventana: comic robots
                    newNode.setIDWindowsEvent(6);
                    break;
                case (30): //Ventana: comic coche bomba
                    newNode.setIDWindowsEvent(7);
                    break;
                //                case (17): //Ventana: asesinato
                //                    //Este nodo debería activarse si el jugador ha cogido el paquete
                //                    newNode.setIDWindowsEvent(7);
                //                    break;
                case (20): //Nuevo canal de porno
                    newNode.setPreviousNode(9);
                    break;

                case (21): //Nuevo canal de porno
                    newNode.setPreviousNode(4);
                    break;

                default:
                    newNode.setNextNode(-1);
                    newNode.setPreviousNode(-1);
                    break;
            }
                
/*            if (i == 5) //Este nodo tiene un nodo siguiente obligatorio
            {
                newNode.setNextNode(i + 1);
                Debug.Log("El nodo " + i + " tiene un nodo posterior: " + (i+1));
            }
            if (i == 6) //Este nodo tiene un nodo anterior obligatorio
            {
                Debug.Log("El nodo " + i + " tiene un nodo anterior: " + (i - 1));
                newNode.setPreviousNode(i - 1);
            }
*/
//            newNode.setTodayNews(arrayNews[i]);

            newNode.setTodayNews(Localization.Get(arrayNews[i]));
            newNode.setThisDayActivatesObject(arrayThisDayActivatesObject[i]);

            if (arrayThisDayActivatesObject[i] == true)
                newNode.setObjectToActivate(iIDObjectToActivate++);

            newNode.setWindowHasEvent(arrayWindowsHasEvent[i]);

            Debug.Log("Nuevo nodo metido!!!!!\nNodo: " + i + "\nID: " + newNode.getID() + "\nNoticia: " +  newNode.getTodayNews() + "\nPrevious: " + newNode.getIDPreviousNode() + "\nNext: " + newNode.getIDNextNode() + "\nIDObject To Activate: " + newNode.getIDObjectToActivate() + "\nWindow has event: " + newNode.getWindowHasEvent() +  "\nWindow has event: " + newNode.getWindowHasEvent() + "\nID Window Sprite: " + newNode.getIDWindowsEvent());

            listOfNews.Add(newNode);
        }
    }

    public void startArrayWindowNews() //TODO: esta función sirve para mostrar mensajes de texto junto al cómic. A lo mejor la quito.
    {
        arrayWindowEvents[iWindowNewsSize - 1] = Localization.Get("Window000");
    }

    public void RemoveLastDay()
    {
        listOfNews.RemoveAt(iCurrentNode);
    }

    public void GetLastDayDEMO()
    {
        iCurrentNode = -1;
        textNews.text = Localization.Get("NewsFinal");
    }
    
    public void StopAllComicSounds()
    {
        for (int i = 0; i< soundComics.Length; i++)
        {
            soundComics[i].Stop();
        }
    }

    public void GetAllDataFromDay(int iDay)
    {
        Debug.Log("Obteniendo los datos del día-nodo: " + iDay + "!!!!!! Y el current day es: " +iCurrentNode);
        Debug.Log("\nID: " + listOfNews[iDay].getID() + "\nNoticia: " + listOfNews[iDay].getTodayNews() + "\nPrevious: " + listOfNews[iDay].getIDPreviousNode() + "\nNext: " + listOfNews[iDay].getIDNextNode() + "\nIDObject To Activate: " + listOfNews[iDay].getIDObjectToActivate() + "\nWindow has event: " + listOfNews[iDay].getWindowHasEvent());

        if (listOfNews[iDay].getThisDayActivatesObject() == true)
            myGameManager.ActivateObject(listOfNews[iDay].getIDObjectToActivate());

        if (listOfNews[iDay].getIDNextNode() >= 0)
        {
            bHasNextNode = true;
            iNextNodeToInsert = listOfNews[iDay].getIDNextNode();
        }

        //TODO no comprobar en todos los días
        switch (listOfNews[iDay].getID())
        {
            case 5: //Ventana: manifestación
                setPoliceLightActive(true);
                soundComics[(int)enumAudioComics.RIOTS].Play();
//                soundRiots.Play();
                break;
            case 7: //Ventana: Control
                setPoliceLightActive(true);
                soundComics[(int)enumAudioComics.POLICE_CONTROL].Play();
//                soundPoliceControl.Play();
                break;
            case 12: //Ventana: Expulsion
                setPoliceLightActive(true);
                soundComics[(int)enumAudioComics.INMIGRANTS].Play();
//                soundInmigrationControl.Play();
                break;
            case 13: //Ventana: Acto
                setPoliceLightActive(true);
                soundComics[(int)enumAudioComics.SERVICE].Play();
//                soundService.Play();
                break;
            case 14: //Ventana: Comida
                setPoliceLightActive(true);
                soundComics[(int)enumAudioComics.FOOD].Play();
                //                soundPoliceControl.Play();
                break;
            case 15: //Ventana: Mitin
                setPoliceLightActive(true);
                soundComics[(int)enumAudioComics.POLITICAL_SPEECH].Play();
                //                soundPoliticalSpeech.Play();
                break;
            case 16: //Ventana: Robots
                setPoliceLightActive(true);
                soundComics[(int)enumAudioComics.ROBOTS].Play();
//                soundPoliceControl.Play();
                break;
            case 30: //Ventana: Robots
                setPoliceLightActive(true);
                soundComics[(int)enumAudioComics.CAR_BOMB].Play();
                //                soundPoliceControl.Play();
                break;
            default:
                setPoliceLightActive(false);
                StopAllComicSounds();
                break;
        }

        arrayNodesUsed[listOfNews[iDay].getID()] = true;
    }

    public void getNewsFromDay()
    {
        Debug.Log("HE ENTRADO EN LAS NOTICIAS DEL DÍA!!!!!!!!");

        if (bHasNextNode == true) //Hay un nodo obligatorio para mostrar
        {
            Debug.Log("Vengo del nodo anterior y soy el nodo " + iNextNodeToInsert);
            int iSelectedNode = -1;
            for (int i = 0; i < listOfNews.Count; i++)
            {
                if (listOfNews[i].getID() == iNextNodeToInsert)
                    iSelectedNode = i;
            }

            Debug.Log("!!!!!!!!Noticia dia "  + listOfNews[iSelectedNode].getTodayNews() +
                "\n La ventana tiene evento: " + listOfNews[iSelectedNode].getWindowHasEvent() +
                "\nActivar objeto: " + listOfNews[iSelectedNode].getThisDayActivatesObject() +
                "\nObjeto para activar: " + listOfNews[iSelectedNode].getIDObjectToActivate() +
                "\n");

            iCurrentNode = iSelectedNode;
            GetAllDataFromDay(iCurrentNode);

//            arrayNodesUsed[listOfNews[iCurrentNode].getID()] = true;
//            listOfNews.RemoveAt(iCurrentNode);

            bHasNextNode = false;
            iNextNodeToInsert = -1;
        }
        else //No hay nodo obligatorio para mostrar
        {
            int iRandom = Random.Range(0, listOfNews.Count);
            int iTemp = 0;

            while (listOfNews[iRandom].getIDPreviousNode() > 0) //Repito esto para comprobar si tiene nodos previos
            {
                if (arrayNodesUsed[listOfNews[iRandom].getIDPreviousNode()] == false) //Si tiene un nodo previo, compruebo si ha salido (debe haber salido)
                {
                    do
                    {
                        iTemp = iRandom;
                        iRandom = Random.Range(0, listOfNews.Count);
                    } while (iTemp == iRandom); //Se comprueba que el valor aleatorio no se repita
                }
                else
                    break;
            } 


            iCurrentNode = iRandom;

/*
            if (listOfNews[iCurrentNode].getIDNextNode() > 0)
            {
                bHasNextNode = true;
                iNextNodeToInsert = listOfNews[iCurrentNode].getIDNextNode();
                Debug.Log("El nodo " + iCurrentNode + " tiene un nodo siguiente obligatorio" + iNextNodeToInsert);
            }
*/
            Debug.Log("!!!!!!!!Noticia dia "+ ": " + listOfNews[iRandom].getTodayNews() +
            "\n La ventana tiene evento: " + listOfNews[iRandom].getWindowHasEvent() +
            "\nActivar objeto: " + listOfNews[iRandom].getThisDayActivatesObject() +
            "\nObjeto para activar: " + listOfNews[iRandom].getIDObjectToActivate() +
            "\n");


//            arrayNodesUsed[listOfNews[iRandom].getID()] = true;
//            listOfNews.RemoveAt(iRandom);

        }

        Debug.Log("Día-nodo seleccionado: " + iCurrentNode);

        GetAllDataFromDay(iCurrentNode);    

        //        iSelectedNode = -1;
 //       return sAnswer;
    }

    public bool getWindowHasEvent()
    {
        bool bAnswer = false;
        {
            //           bAnswer = arrayWindowsHasEvent[iDay];
            bAnswer = listOfNews[iCurrentNode].getWindowHasEvent();
//                        bAnswer = listOfNews[iSelectedNode].getWindowHasEvent();
        }
        Debug.Log("El día " + iCurrentNode + " hay evento en ventana: " + bAnswer);
        return bAnswer;
    }

    public string getEventFromWindow()
    {
//        Debug.Log("Obteniendo el evento ded la ventana!!!!");
        string sTemp = "";
        int iIndexTemp = getIndexNodeWithID(listOfNews[iCurrentNode].getIDNextNode());

//        Debug.Log("El valor del día actual es " + iCurrentNode + " y del iIndexTemp es: " + iIndexTemp + "!!!!!!\n" );

        if (iIndexTemp >= 0) //Si hay un nodo obligatorio siguiente
        {
            switch (listOfNews[iCurrentNode].getID())
            {
                case 5:
                    listOfNews[iIndexTemp].setTodayNews(Localization.Get("News006b"));
                    listOfNews[iIndexTemp].setThisDayActivatesObject(false);
                    break;
            }
        }

        StartCoroutine(showComic());

        return sTemp;
    }

    public bool getIsObjectToday(int iDay)
    {
        return arrayThisDayActivatesObject[iDay];
    }

    public void setPoliceLightActive(bool bNewValue)
    {
        goPoliceLights.SetActive(bNewValue);
    }

    public void setEndGameTerroristStrike()
    {
        dystopicGameManager.myGameManagerInstance.EndGame((int)dystopicGameManager.EnumTypeOfEnd.DISIDENT);
    }

    public void addObjectsActivatedCounter()
    {
        iObjectsActivatedCounter++;
        Debug.Log("Activando el objeto: " + iObjectsActivatedCounter);
    }

    public void showNews()
    {
        goNews.SetActive(true);
        //        textNews.text = getNewsFromDay(iDay);
        //        textNews.text = listOfNews[iCurrentNode].getTodayNews();
        if(iCurrentNode != -1)
        {
            textNews.text = listOfNews[iCurrentNode].getTodayNews();
//            Debug.Log("Estoy mostrando las noticias de hoy, nodo " + iCurrentNode + ": " + listOfNews[iCurrentNode].getTodayNews() + " " + listOfNews[iCurrentNode].getID());
        }
        StartCoroutine(ActivateNewsButton());
    }

    public void hideNews()
    {
        dystopicGameManager.myGameManagerInstance.ResumeGame();
        goNews.SetActive(false);
        textNews.text = "";
        buttonImageNews.enabled = false;
//        textNews.enabled = false;
//        imageNews.enabled = false;
    }

    public void hideFinalNews()
    {
        dystopicGameManager.myGameManagerInstance.ResumeGame();
        goFinalNews.SetActive(false);
        textFinalNews.text = "";
        buttonImageFinalNews.enabled = false;
        dystopicGameManager.myGameManagerInstance.ExitGame();
//        textNews.enabled = false;
//        imageNews.enabled = false;
    }

    public void ActivateNewsButtonFromOutside()
    {
        StartCoroutine(ActivateNewsButton());
    }

    public void ActivateFinalNewsButtonFromOutside()
    {
        StartCoroutine(ActivateFinalNewsButton());
    }


    IEnumerator showComic()
    {
        int iIDTEMP = listOfNews[iCurrentNode].getIDWindowsEvent(); //ID del evento de la ventana para activar cómic y Afinidad Política
        setPoliticalAffinityTEMP(afPoliticalAffinity[iIDTEMP]);

        yield return new WaitForSeconds(0.05f);
//        arrayGoSpritesWindow[iNewsFromWindowCounter].enabled = true;
        dystopicGameManager.myGameManagerInstance.PauseGame();
        goBackgroundComicWindow.SetActive(true);

        arrayGoSpritesWindow[iIDTEMP].SetActive(true);

        iNewsFromWindowCounter++;
    }

    IEnumerator ActivateNewsButton()
    {
        yield return new WaitForSeconds(0.05f);
        buttonImageNews.enabled = true;
        dystopicGameManager.myGameManagerInstance.PauseGame();
    }
    IEnumerator ActivateFinalNewsButton()
    {
        yield return new WaitForSeconds(0.05f);
        buttonImageFinalNews.enabled = true;

        dystopicGameManager.myGameManagerInstance.PauseGame();
    }

    IEnumerator ActivateNewsButton(GameObject gameObjectAfected)
    {
        yield return new WaitForSeconds(0.05f);
        gameObjectAfected.SetActive(true);
        dystopicGameManager.myGameManagerInstance.PauseGame();
    }

    // Use this for initialization
    void Start()
    {
        goNews.SetActive(false);
        buttonImageNews.enabled = false;

        goFinalNews.SetActive(false);

        arrayNodesUsed = new bool[arrayNews.Length];

        for (int i = 0; i < arrayNodesUsed.Length; i++)
        {
            arrayNodesUsed[i] = false;
        }

        //        imageNews.enabled = false;
        //        textNews.enabled = false;

        //        arrayNews = new string[iNewsSize];
//        arrayGoSpritesWindow = goBackgroundComicWindow.GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < arrayGoSpritesWindow.Length; i++)
        {
            arrayGoSpritesWindow[i].SetActive(false);
        }

        goBackgroundComicWindow.SetActive(false);

        arrayWindowsHasEvent = new bool[iNewsSize];
        arrayWindowEvents = new string[iWindowNewsSize];

        arrayThisDayActivatesObject = new bool[iNewsSize];
        arrayIDObjects = new int[iObjectsToActivate];

        startArrayNews();
        startArrayWindowNews();
//        startArrayObjectsToActivate();

        setPoliceLightActive(false);
    }
}
