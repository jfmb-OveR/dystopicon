﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rat_Behaviour : MonoBehaviour {
    public MoveToDestination myMove;
    public GameObject[] goDestinations;
    public float fSpeed;
    public GameObject goSoundRatWalking;

    private int iLength;
    private GameObject goTemp;

    private Animator animRat;
    private int iRatHash;

    private Collider colRat;

    private bool bRatWalking = false;

    public void WalkRat(bool bValue)
    {
        bRatWalking = bValue;
        myMove.setMove(bValue);
//        goSoundRatWalking.SetActive(bValue);
        animRat.SetBool(iRatHash, bValue);
        colRat.enabled = bValue;
    }

    private void OnMouseDown()
    {
        myMove.setSpeed(0f);
        myMove.setMove(false);
//        colRat.enabled = false;
    }

    IEnumerator CorStartRat()
    {
        goTemp = goDestinations[Random.Range(0, goDestinations.Length)];
        WalkRat(false);

        yield return new WaitForSeconds(Random.Range(5,15));
        this.gameObject.transform.LookAt(goTemp.transform.position);
        myMove.setDestino(goTemp.transform.position);
        myMove.setArrived(false);

        WalkRat(true);
/*
        bRatWalking = true;
        myMove.setMove(true);
        animRat.SetBool(iRatHash, true);
        myRat.UnPauseSound();
        */
    }

    // Use this for initialization
    void Awake () {
        iLength = goDestinations.Length;

        animRat = this.GetComponent<Animator>();
        iRatHash = Animator.StringToHash("isWalking");
        animRat.SetBool(iRatHash, false);

        this.gameObject.transform.position = goDestinations[Random.Range(0, iLength)].transform.position;
        myMove.setSpeed(fSpeed);

        colRat = this.gameObject.GetComponent<Collider>();

        StartCoroutine(CorStartRat());
	}

    private void Update()
    {
        if(myMove.getArrived() == true)
        {
            if(bRatWalking == true)
            {
                StartCoroutine(CorStartRat());
            }
        }
    }
}
