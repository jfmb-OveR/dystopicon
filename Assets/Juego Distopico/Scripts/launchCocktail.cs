﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class launchCocktail : MonoBehaviour {

    public GameObject goCocktail;

    private void Awake()
    {
        if(goCocktail != null)
            LaunchCocktail();
    }
    public void LaunchCocktail()
    {
        StartCoroutine(LaunchingCocktail());
    }

    IEnumerator LaunchingCocktail()
    {
        yield return new WaitForSeconds(5);
        goCocktail.SetActive(true);
    }
}
