﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmarioScript : MonoBehaviour {
    public GameObject goStar;
    private Animator animDevice;
    private int isOpenHash;
    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;

    private bool bIsActive = false;

    public void CheckBeforeActivate()
    {
        if (bIsActive == true)
            SetActiveCollider();
    }

    public void SetActiveCollider()
    {
        this.gameObject.GetComponent<BoxCollider>().enabled = true;
    }

    public void ActivateObject()
    {
        bIsActive = true;
        SetActiveCollider();
        goStar.SetActive(true);
    }

    void OnMouseDown()
    {
        animDevice.SetBool(isOpenHash, true);
        bIsActive = false;
        goStar.SetActive(false);
        StartCoroutine(CorDisableAnimator());
    }


    private void OnMouseOver()
    {
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);
    }

    IEnumerator CorDisableAnimator()
    {
        yield return new WaitForSeconds(3);
        animDevice.enabled = false;
    }

    private void Awake()
    {
        goStar.SetActive(false);
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
        animDevice = this.GetComponent<Animator>();
        isOpenHash = Animator.StringToHash("openDevice");
    }
}
