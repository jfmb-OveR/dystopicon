﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class useTV_v2 : MonoBehaviour {
    public int iIndex;
    public myAnalyticsScriptForDevices myAnalytics;

    public Texture2D textureCursorNormal;
    public Texture2D textureCursorOver;

    public characterManager myCharacterManager;
    public float fMoneyModifier;
    public float fValueModifier;

    public Material materialTV;
    public dystopicGameManager myGameManager;
    [Header("Politics, Religion, Entert., News, Sports, Porn")]
    public GameObject[] arrayChannelsSounds;

    private bool bTVOn = false;

    public Image imageChannels;
    public Text textDevice;
    public UILabel labelDevices;

    private Animator animDevice;
    private int isOpenHash;

    private float fPoliticalModifier;

    private int iTimesClicked;

    public enum EnumChannelSounds
    {
        POLITICS = 0,
        RELIGION = 1,
        ENTERTAIMENT = 2,
        NEWS = 3,
        SPORT = 4,
        PORN = 5
    }


    public float getCost()
    {
        return fMoneyModifier;
    }

    public bool getTVOn()
    {
        return bTVOn;
    }

    public void setPoliticalModifier(float fNewModifier)
    {
        fPoliticalModifier = fNewModifier;
    }

    public void setMoneyModifier(float fNewValue)
    {
        fMoneyModifier = fNewValue;
    }

    public void setFunValueModifier(float fNewValue)
    {
        fValueModifier = fNewValue;
    }

    public void setTVOff()
    {
        bTVOn = false;

        materialTV.DisableKeyword("_EMISSION");
        SetColorGrey();
        TurnOFFAllSounds();

        animDevice.SetBool(isOpenHash, false);

        setMoneyModifier(0);
        setFunValueModifier(0);
        //        myCharacterManager.setModifyMoney(fMoneyModifier);
        //        myCharacterManager.setMoreFun(fValueModifier);
        sendInfoToCharacterManager();

//        myCharacterManager.setIdleState();
    }

    public void setTVOn()
    {
        bTVOn = true;
        materialTV.EnableKeyword("_EMISSION");

        animDevice.SetBool(isOpenHash, true);

        sendInfoToCharacterManager();
        //            myCharacterManager.setModifyMoney(fMoneyModifier);
        //            myCharacterManager.setMoreFun(fValueModifier);
        myCharacterManager.setTVActive();
    }

    private void TurnOFFAllSounds()
    {
        arrayChannelsSounds[(int)EnumChannelSounds.POLITICS].SetActive(false);
        arrayChannelsSounds[(int)EnumChannelSounds.ENTERTAIMENT].SetActive(false);
        arrayChannelsSounds[(int)EnumChannelSounds.RELIGION].SetActive(false);
        arrayChannelsSounds[(int)EnumChannelSounds.NEWS].SetActive(false);
        arrayChannelsSounds[(int)EnumChannelSounds.SPORT].SetActive(false);
        arrayChannelsSounds[(int)EnumChannelSounds.PORN].SetActive(false);
    }

#region Colors
public void SetColorRed() //Politics
    {
        TurnOFFAllSounds();
        materialTV.color = Color.red;
        arrayChannelsSounds[(int)EnumChannelSounds.POLITICS].SetActive(true);
    }

    public void SetColorBlue() //Entertaiment
    {
        TurnOFFAllSounds();
        materialTV.color = Color.blue;
        arrayChannelsSounds[(int)EnumChannelSounds.ENTERTAIMENT].SetActive(true);
    }

    public void SetColorYellow() //religion
    {
        TurnOFFAllSounds();
        materialTV.color = Color.yellow;
        arrayChannelsSounds[(int)EnumChannelSounds.RELIGION].SetActive(true);
    }

    public void SetColorBlack() //News
    {
        TurnOFFAllSounds();
        materialTV.color = Color.black;
        arrayChannelsSounds[(int)EnumChannelSounds.NEWS].SetActive(true);
    }

    public void SetColorGreen() //Sports
    {
        TurnOFFAllSounds();
        materialTV.color = Color.green;
        arrayChannelsSounds[(int)EnumChannelSounds.SPORT].SetActive(true);
    }

    public void SetColorMagenta() //Porn
    {
        TurnOFFAllSounds();
        materialTV.color = Color.magenta;
        arrayChannelsSounds[(int)EnumChannelSounds.PORN].SetActive(true);
    }

    public void SetColorWhite()
    {
        materialTV.color = Color.white;
    }

    public void SetColorGrey()
    {
        materialTV.color = Color.grey;
    }
    #endregion

    public void sendInfoToCharacterManager()
    {
        myCharacterManager.setModifyMoney(fMoneyModifier);
        myCharacterManager.setExternalFunModifierWithoutSeconds(fValueModifier);
        myCharacterManager.sumaPoliticalAffinity(fPoliticalModifier);
    }


    public void hideChannelsImage()
    {
        imageChannels.gameObject.SetActive(false);
    }

    public void showHideChannelsImage()
    {
        bool bValue = !imageChannels.gameObject.activeSelf;
//        if (mySofa.getIsActive() == true)
            imageChannels.gameObject.SetActive(bValue);
            myGameManager.ActivateDeactivateCollidersExceptTV(!bValue);
    }


    private void OnMouseOver()
    {
        Cursor.SetCursor(textureCursorOver, Vector2.zero, CursorMode.Auto);

        //        textDevice.enabled = true;
        labelDevices.enabled = true;
    }

    private void OnMouseExit()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        //        textDevice.enabled = false;
        labelDevices.enabled = false;
    }

    void OnMouseDown()
    {
        Cursor.SetCursor(textureCursorNormal, Vector2.zero, CursorMode.Auto);

        if (myCharacterManager.getIsTVAllowed() == true)
        {
            myAnalytics.increaseTimeUsed();

            showHideChannelsImage();
        }
    }

    void Awake()
    {
        //        imageChannels.enabled = false;
        imageChannels.gameObject.SetActive(false);
        //        materialTV = this.GetComponent<Material>();

        materialTV.DisableKeyword("_EMISSION");
        SetColorGrey();

//        textDevice.enabled = false;
        labelDevices.enabled = false;

        animDevice = this.GetComponent<Animator>();
        isOpenHash = Animator.StringToHash("openDevice");

        setTVOff();
    }
}
