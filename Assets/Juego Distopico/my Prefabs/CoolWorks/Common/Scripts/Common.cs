﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoolWorks
{
    public static class Common
    {
        public static float ClampAngle(float angle)
        {
            return (angle % 360) + (angle < 0 ? 360 : 0);
        }
    }
}
