﻿using UnityEngine;
using System.Collections;

namespace CoolWorks
{
    public class Menu : MonoBehaviour
    {
        public void Start()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        public void LoadScene(string name)
        {
            Application.LoadLevel(name);
        }
    }
}
